---
tags:
  - typo3
  - typoscript
  - lib
  - "#fluid"
---
## Content elements

Use lib to place content elements by `pidInList` (comma separated list) and `uidInList` (comma separated list) into fluid templates.

### lib

	lib.contentElements = COA
	lib.contentElements {
	    5 = LOAD_REGISTER
	    5  {
	        uidInList.cObject = TEXT
	        uidInList.cObject {
	            field = uidInList
	            ifEmpty.data = this
	        }
	        pidInList.cObject = TEXT
	        pidInList.cObject {
	            field = pidInList
	            ifEmpty.data = this
	        }
	    }
		
	    20 = CONTENT
	    20 {
	        table = tt_content
	        select {
	            orderBy = sorting
	            where = uid IN({register:uidInList})
	            where.insertData = 1
	            pidInList.data = register:pidInList
	        }
	    }
	
	    90 = RESTORE_REGISTER
	}

### Fluid template

foo: comma separated list of uids
bar: comma separated list of pids
	
	<f:cObject 
		typoscriptObjectPath="lib.contentElements" 
		data="{uidInList: 'foo', pidInList: 'bar'}" />