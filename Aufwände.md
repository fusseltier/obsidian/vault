1. **Einrichtung Projekt** `P.F001` - `P.F008`
	1. Lokal (Development)
		1. Docker Base Project
		2. Pimcore Skeleton
		3. Konfiguration
	2. Online (Production)
		1. Server in der Hetzner Cloud
			1. Traefik Proxy
			2. Docker Base Project
			3. Pimcore Skeleton
			4. Konfiguration
2. **Datenmodell** `P.D001`
	1. Abbildung der Domäne
		1. Definition aller Objekte
		2. Anlegen aller Objekte im Pimcore Backend
		3. Installer für alle Objekte
3. **Implementierung**
	1. Template
		1. Grund Layout `P.D002` , ` P.D003`,  `P.D007`
		2. Startseite `P.D004` - `P.D006`, `P.D008`, `P.D019`
		3. Produkt `P.D009` - `P.D015`
		4. Warenkorb `P.D016` - `P.D018`
			1. Beratung & Angebot `P.D022`, `P.D023`
				1. Mail `P.D022`, `P.D023`
				2. CSV (als Mailanhang) `P.d024`
			2. Code (Warenkorb speichern) `P.D021`
			3. PDF (Warenkorb als PDF speichern) `P.D020`
	2. Logik 
		1. Startseite `P.D004` - `P.D006`, `P.D008`, `P.D019`
		2. Produkt `P.D009` - `P.D015`
		3. Warenkorb `P.D016` - `P.D018`
			1. Beratung & Angebot `P.D022`, `P.D023`
				1. Mail `P.D022`, `P.D023`
				2. CSV (als Mailanhang) `P.d024`
			2. Code (Warenkorb speichern) `P.D021`
			3. PDF (Warenkorb als PDF speichern) `P.D020`


Wie kann der Fehler reproduziert werden:

Der Code wird wie folgt eingebunden:

```html
<head>
	<script>
	(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-WRZGJV7');
	</script>
</head>
```

Der User hat den statistischen Cookies nicht zugestimmt. Der Consent Cookie `cookiemanager_selection` ist noch nicht gesetzt bzw. enthält nicht den Wert`,statistical`. Es werden folgende Parameter an die URL angehängt:

`?_gl=1*jmidxr*_up*MQ..*_ga*OTIxODc2OTEuMTY3NjY0NjU5Ng..*_ga_9LRM66H688*MTY3NjY0NjU5NS4xLjAuMTY3NjY0NjU5NS4wLjAuMA..`

Der User hat den statistischen Cookies zugestimmt. Es werden keine Parameter an die URL angehängt. 

Wird der Tagmanger nicht eingebunden, werden die Parameter unabhängig vom Cookie Consent nie an die URL angehängt. Daher kommen wir zu der Schlussfolgerung, dass diese Parameter über den Tagmanager angehängt werden, sobald der User auf einen beliebigen link klickt.

