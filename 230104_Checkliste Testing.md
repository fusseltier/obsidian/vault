<sub>**Stand: 230104**</sub>
# Checkliste Testing
## Einleitung

Grundsätzlich gibt es zwei wesentliche Gesichtspunkte, die getestet werden sollten:

- Funktionalität
- Optik

Dabei sollten diese jeweils auf dem **Desktop** beispielsweise mit der jeweils aktuellen Version von Chrome, Edge und Firefox, sowie auf **Smartphones** beispielsweise mit der jeweils aktuellen Version von Chrome, Samsung Internet und Safari getestet werden.

Das Testing sollte alle Bereiche des Intranets umfassen.

- Grundaufbau
	- Navigation
	- Header (Feedback, Login, Suche)
- Seiten
	- Dashboard
	- Termine
	- Mitteilungen
	- Kontakte (sobald implementiert)
	- Informationen (Content Seiten)
	- Login / Logout
	- Suche
	- Feedback (Formular)

## Funktionalität
- Login über Azure AD / Logout 
      Mit verschiedenen Benutzern, die unterschiedliche Berechtigungen haben
- Filter
	- Termine
	- Mitteilungen
	- Kontakte
- Downloads
	-In den Informationen (Content Seiten)
	- In den Bereichen Termine und Mitteilungen
- Formulare
	- Feedback
- Navigation
	- Hauptnavigation
	- Boxen im Content
	- Sidebar Dashboard
- Suche

## Optik
- Darstellung bspw. 
	- Abstände
	- Korrekte Darstellung


<div style="page-break-after: always;"></div>

## Dokumentation von Fehlern
Bitte dokumentieren Sie Fehler anhand folgender Vorlage:

>Aussagekräftiger Titel
>
>**Deeplink**
>
>Link zur betroffenen Seite
>
>**IST | fehlerhaftes Verhalten**
>
>Kurze Beschreibung des fehlerhaften Verhaltens und wie dieses reproduziert werden kann.
>
>**SOLL | erwartetes Verhalten**
>
>Kurze beschreibung des erwarteten Verhaltens, beispielsweise bei einem Fehler einer Funktionalität.
>
>**Angaben zum verwendeten Gerät**
>- Verwendeter Browser inkl. Version
>- Verwendetes Betriebssystem inkl. Version
>- Smartphone oder Desktop