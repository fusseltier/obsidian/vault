#ed25519 #rsa #ssh #ssh2

ssh-rsa does not work with newer OPENSSH versions!

The [release notes](https://www.openssh.com/releasenotes.html) for OpenSSH 9.1 state:

```
This release disables RSA signatures using the SHA-1 hash algorithm
by default. This change has been made as the SHA-1 hash algorithm is
cryptographically broken, and it is possible to create chosen-prefix
hash collisions for <USD$50K [1]
```

To work around this, you could edit `/etc/ssh/sshd_config` on the Ventura server and add this line to allow use of your existing key:

```
PubkeyAcceptedAlgorithms +ssh-rsa
```

Or, you could create a new key using a different algorithm. Arq 7 supports ed25519 keys, so you could

```
ssh-keygen -t ed25519 
```

to create a new public/private key pair. Add the public key to your account’s `~/.ssh/authorized_keys` file on the Ventura server, and configure a storage location in Arq using the private key.