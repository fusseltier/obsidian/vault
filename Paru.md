#makepkg #fix #arch #linux #bash #rebuild

## Error

```
paru: error while loading shared libraries: libssl.so.1.1: cannot open shared object file: No such file or directory
```

## Fix

```
sudo pacman -S openssl-1.1
```

## Rebuild a package

```
cd ~/.cache/paru/clone/paru
makepkg -si
```