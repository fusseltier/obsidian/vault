If docker daemon does not start:
```bash
sudo dockerd --debug
```

See: https://stackoverflow.com/questions/55906503/docker-how-to-fix-job-for-docker-service-failed-because-the-control-process-ex