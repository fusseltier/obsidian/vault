**Stand 221212**
## 4.1

> [!danger] Severity
> high

### 4.1. Missing Updates (OpenSSH)
	
- [ ] Resolved
- [x] Partially resolved
- [ ] Not resolved

TicketID: JAGF-0780-JVZQ 

Our server is running AlmaLinux 8.7 (based on Red Hat Enterprise Linux). This means that the mentioned CVEs have been partially closed.

#### In detail:

- [x] **CVE-2019-16905** ist bei AlmaLinux Systemen nicht betroffen: https://access.redhat.com/security/cve/cve-2019-16905
- [x] **CVE-2021-41617** wurde behoben: https://errata.almalinux.org/8/ALSA-2022-2013.html

**We cannot resolve the other two CVEs.** Please read the explanations from Red Hat.

- [ ] **CVE-2020-15778**: ""\[...\] In order to exploit this flaw, the attacker needs to social engineer or manipulate a system administrator (who has root access on the remote server) to run scp with a malicious command line parameter. \[...\]"
     More information: https://access.redhat.com/security/cve/cve-2020-15778
 - [ ] **CVE-2016-20012** "\[...\] Although a CVE was assigned upstream and Red Hat doesn't consider it to be a security flaw and won't receive any patch, also the CVE was made as disputed by MITRE. Considering that Red Hat is closing this flaw as NOTABUG.\[...\]" 
     More: information: https://access.redhat.com/security/cve/cve-2016-20012

## 4.2. - 4.12.

> [!fail] Severity
> medium

### 4.2. Manipulatable email body

- [x] Resolved
- [ ] Partially resolved
- [ ] Not resolved

Resolved by adding sanitizing to the form fields in the application.

### 4.3. X-Powered-By misconfiguration

- [x] Resolved
- [ ] Partially resolved
- [ ] Not resolved

This was caused by a bad apache default configuration. After changing the configuration the application now throws an 404 error if visiting pathes like /icons, /icons/README or /icons/small.

### 4.4. Web server default components

- [x] Resolved
- [ ] Partially resolved
- [ ] Not resolved

See [[Report#4.3. X-Powered-By misconfiguration]]

### 4.5. Unnecessary development files

- [x] Resolved
- [ ] Partially resolved
- [ ] Not resolved

This was caused by a bad apache default configuration. After changing the configuration the application now throws an 404 error if visiting pathes like /awstats or /cgi-bin.

### 4.6. Exposure of sensitive services

- [x] Resolved
- [ ] Partially resolved
- [ ] Not resolved

We closed some of the ports mentions. A current list of open ports. Ports 8443, 8880 and 7088 are filtered:

```
PORT     STATE SERVICE
21/tcp   open  ftp
22/tcp   open  ssh
80/tcp   open  http
443/tcp  open  https
7088/tcp filtered zixi-transport
8443/tcp filtered https-alt
8880/tcp filtered cddbp-alt
```

### 4.7. Missing Updates (PHP)

- [x] Resolved
- [ ] Partially resolved
- [ ] Not resolved

See [[Report#4.3. X-Powered-By misconfiguration]]

### 4.8. Unvalidated redirect using an arbitrary Host-Header

- [x] Resolved
- [ ] Partially resolved
- [ ] Not resolved

Closed ticket: **Ticket ID:  HAPO-7782-EGQB**

### 4.9. Protocol to replace (TLS)

- [x] Resolved
- [ ] Partially resolved
- [ ] Not resolved

After changing some configurtations only TLSv1.2 and TLSv1.3 ant the the ciphers shown below are in use.

```
$ nmap --script ssl-enum-ciphers -p 443 staging.cyberconference.schwarz.zwei14.website

[...]

PORT    STATE SERVICE
443/tcp open  https
| ssl-enum-ciphers: 
|   TLSv1.2: 
|     ciphers: 
|       TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256 (ecdh_x25519) - A
|       TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384 (ecdh_x25519) - A
|       TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305_SHA256 (ecdh_x25519) - A
|     compressors: 
|       NULL
|     cipher preference: server
|   TLSv1.3: 
|     ciphers: 
|       TLS_AKE_WITH_AES_128_GCM_SHA256 (ecdh_x25519) - A
|       TLS_AKE_WITH_AES_256_GCM_SHA384 (ecdh_x25519) - A
|       TLS_AKE_WITH_CHACHA20_POLY1305_SHA256 (ecdh_x25519) - A
|     cipher preference: client
|_  least strength: A
```

### 4.10. Cipher to replace (TLS)

- [x] Resolved
- [ ] Partially resolved
- [ ] Not resolved

See [[Report#4.9. Protocol to replace (TLS)]]

### 4.11. Weak SSH algorithm

- [x] Resolved
- [ ] Partially resolved
- [ ] Not resolved

All cryptographic methods and algorithms listed with \[fail\] or \[warn\] by `ssh-audit staging.cyberconference.schwarz.zwei14.website` are now disabled.

### 4.12. Outdated SSH algorithm

- [x] Resolved
- [ ] Partially resolved
- [ ] Not resolved

See [[Report#4.11. Weak SSH algorithm]]

## 4.13. - 4.16.

> [!warning] Severity
> low

### 4.13 Missing Expect-CT-Header

- [x] Resolved
- [ ] Partially resolved
- [] Not resolved

Header set via .htaccess:

```
Header always set Expect-CT "max-age=0; report-uri=https://cyberconference.schwarz/reportOnly"
```

This feature is deprecated and no longer recommended according to Mozilla. See detailed informations below:

"**Deprecated:** This feature is no longer recommended. Though some browsers might still support it, it may have already been removed from the relevant web standards, may be in the process of being dropped, or may only be kept for compatibility purposes. Avoid using it, and update existing code if possible; see the [compatibility table](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Expect-CT#browser_compatibility) at the bottom of this page to guide your decision. Be aware that this feature may cease to work at any time."
(https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Expect-CT)

### 4.14. Insecure Content-Security-Policy

- [x] Resolved
- [ ] Partially resolved
- [ ] Not resolved

Replaced "unsafe-inline" with "nonce='{SERVER-GENERATED-NONCE}" . See https://developers.google.com/tag-platform/tag-manager/web/csp


### 4.15. Missing OCSP stapling

- [x] Resolved
- [ ] Partially resolved
- [ ] Not resolved

Closed ticket: **Ticket ID:  QLQF-5602-WLWV**

### 4.16. DKIM not used for emails

- [x] Resolved
- [ ] Partially resolved
- [ ] Not resolved

## 4.17. - 4.26

> [!info] Severity
> none

### 4.17. Cross-domain script inclusion

- [ ] Resolved
- [ ] Partially resolved
- [x] Not resolved

Using e.g. external scripts from cdn.cookielaw.org or googletagmanager.com is a requirement. Cannot be removed. Therefor marked as "Not resolved".

### 4.18. Referrer misconfiguration

- [x] Resolved
- [ ] Partially resolved
- [ ] Not resolved

### 4.19. Insecure Access-Control-Allow-Origin policy

- [x] Resolved
- [ ] Partially resolved
- [ ] Not resolved

### 4.20. Using SSH with password-based authentication

- [x] Resolved
- [ ] Partially resolved
- [ ] Not resolved

password-based authentication is only allowed from specific IPs. 

### 4.21. Using the default port for SSH

- [ ] Resolved
- [ ] Partially resolved
- [x] Not resolved

Not resolved cause all open ports could easily be listet via nmap or other tools. We consider the relevance for security to be limited.
 
### 4.22. Business-logic filtering on the client

- [ ] Resolved
- [ ] Partially resolved
- [x] Not resolved

See [[Report#4.2. Manipulatable email body]]. Also max characters for all fields are implemented in the application.

### 4.23. Outdated name used for Permissions-Policy header

- [x] Resolved
- [ ] Partially resolved
- [ ] Not resolved

### 4.24. Misconfigured Permissions-Policy-Header

- [x] Resolved
- [ ] Partially resolved
- [ ] Not resolved

### 4.25. Missing security attributes for links to foreign domains

- [ ] Resolved
- [x] Partially resolved
- [ ] Not resolved

Resolved for all external links in Application. External links e.g. set via onetrust cannot be edited by ZWEi14.
### 4.26. Cookie consent not GDPR compliant

- [x] Resolved
- [ ] Partially resolved
- [ ] Not resolved

See also https://jira.schwarz/browse/CRE-2266?filter=-2
See also [[Report#4.17. Cross-domain script inclusion]]
