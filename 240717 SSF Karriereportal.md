- Kein Mehrfachauswahl in den Selectfeldern aber Mehrfachzuordnung in Loga, ein Ergebnis in der Liste
- Arbeitsfeld und Einsatzort: definierte Werte in Select Feld ausgeben, auch wenn es keine Stelle gibt 
- Tätigkeitsbereich: Selectfeld weg, Keine Ausgabe in der Detailansicht
- Einstiegsdatum: nicht als Filter (Select Feld weg), Detailansicht bleibt
- Adresse über Bild, einzeilig, Standortpin + Text, Feld: longtext1
- Startseite Einstiegstext 22px statt 18px

- XML immer mal wieder fehlerhaft: Evtl beim Download auf Syntax prüfen (erst mal abwarten)
- Indeed (zurückstellen?)