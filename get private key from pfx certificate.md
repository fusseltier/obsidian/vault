#ssl #certificate #pfx #key

So lässt sich der Key und das Zertifikat ganz einfach exportieren. Im diesem Beispiel, haben wir unsere exportierte Datei mit dem Namen server.pfx, die wir zuvor vom IIS exportiert haben. Dann kann mit diesem Befehl, der private Schlüssel in die Datei server.key und das Zertifikat nach server.crt exportiert werden:

```
$ openssl pkcs12 -in server.pfx -nocerts -nodes -out server.key  
$ openssl pkcs12 -in server.pfx -clcerts -nokeys -out server.crt
```

Wird noch das Certificate Authority (CA) Zertifikat benötigt, kann dies ebenfalls exportiert werden:

```
$ openssl pkcs12 -in server.pfx -out server-ca.crt -nodes -nokeys -cacerts
```


You can extract an unencrypted private key from a .pfx certificate in bash on Linux using the following command:

```bash
openssl pkcs12 -in certificate.pfx -nocerts -out privateKey.pem -nodes
```

[This command extracts the private key from the .pfx file and saves it in privateKey.pem file as an unencrypted key](https://stackoverflow.com/questions/15413646/converting-pfx-to-pem-using-openssl)[1](https://stackoverflow.com/questions/15413646/converting-pfx-to-pem-using-openssl)[2](https://stackoverflow.com/questions/16397858/how-to-extract-private-key-from-pfx-file-using-openssl)

if getting error like: "digital envelope routines:inner_evp_generic_fetch:unsupported:crypto/evp/evp_fetch.c:355:Global default library context, Algorithm (RC2-40-CBC : 0), Properties ()"

use option  `-legacy` on each command

See: https://man.archlinux.org/man/openssl-pkcs12.1ssl.en

## Final solution for magazin_gestalterbank_de.pfx:

```
openssl pkcs12 -in magazin_gestalterbank_de.pfx -out privateKey.pem -nodes -legacy 
```
