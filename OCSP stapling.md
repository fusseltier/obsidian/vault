#security #litespeed

Add `lsocsp.conf` to `/etc/httpd/conf.d`

Insert 

```
<IfModule Litespeed>
    SSLStaplingCache shmcb:/var/run/ocsp(128000)
    SSLUseStapling on
</IfModule>
```