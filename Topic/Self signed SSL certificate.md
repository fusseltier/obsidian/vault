```bash
openssl req -x509 -newkey rsa:2048 -keyout ./certs/wildcard.selfsignedcert.localhost.key -out ./certs/wildcard.selfsignedcert.localhost.crt -days 3560 -nodes -subj "/C=DE/ST=Bavarian/L=Muenich/O=ZWEI14 GmbH/CN=*.selfsignedcert.localhost"

```

#ssl #certificate #bash #openssl #crt #key