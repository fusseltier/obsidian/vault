#security #ssl #tls #sshd #htaccess

> [!danger] Severity
> high

- [ ] 4.1. Missing Updates (OpenSSH)
	- [ ] Reply from our hoster Dogado:
		- [x] CVE-2019-16905 ist bei AlmaLinux Systemen nicht betroffen: https://access.redhat.com/security/cve/cve-2019-16905
		- [ ] CVE-2020-15778 **wird aus Kompatibilitätsgründen nicht behoben**: https://access.redhat.com/security/cve/cve-2020-15778 (Hier sollte statt scp bspw. rsync verwendet werden)
		- [x] CVE-2021-41617 wurde behoben: https://errata.almalinux.org/8/ALSA-2022-2013.html
		- [ ] CVE-2016-20012 wird **von Redhat als unkritisch eingestuft** und aus dem Grund auch **nicht gefixed**: https://access.redhat.com/security/cve/cve-2016-20012

> [!fail] Severity
> medium

- [ ] 4.2. Manipulatable email body
	- [ ] \[**Dome?**\] Add better sanitizing: e.g dont allow http://, https://, www, dots and urls in general
- [x] 4.3. X-Powered-By misconfiguration
- [x] 4.4. Web server default components
- [x] 4.5. Unnecessary development files
- [ ] 4.6. Exposure of sensitive services
	- [ ] **Besprechen, ob wir noch Ports in der Firewall schließen können**
- [x] 4.7. Missing Updates (PHP)
- [ ] 4.8. Unvalidated redirect using an arbitrary Host-Header
	- [ ] **Ticket offen** \[Ticket ID:  HAPO-7782-EGQB\]
- [x] 4.9. Protocol to replace (TLS)
- [x] 4.10. Cipher to replace (TLS)
- [x] 4.11. Weak SSH algorithm
- [x] 4.12. Outdated SSH algorithm

> [!warning] Severity
> low

- [x] 4.13 Missing Expect-CT-Header
	- [x] \[DEPRECATED\] Not going to implement
- [ ] 4.14. Insecure Content-Security-Policy
	- [x] Remove unsafe-inline: Merge request in place
- [ ] 4.15. Missing OCSP stapling
	- [ ] **Ticket offen:** \[Ticket ID: QLQF-5602-WLWV\]
- [ ] 4.16. DKIM not used for emails
	- [x] \[**Dome?**\] **I think this is arleady solved(?)**

> [!info] Severity
> none

- [ ] 4.17. Cross-domain script inclusion
	- [ ] \[**Dome**\]: cdn.cookielaw.org, The use of that provider may be a requirement from schwarz itself?
- [ ] 4.18. Referrer misconfiguration
- [ ] 4.19. Insecure Access-Control-Allow-Origin policy
	- [x] Merge request
- [ ] 4.20. Using SSH with password-based authentication
	- [ ] Possible: **tbd**
- [ ] 4.21. Using the default port for SSH
	- [ ] Possible: **tbd**
- [ ] 4.22. Business-logic filtering on the client
	- [ ] \[**Dome**\] Maybe sanitize input clientside (max characters, ...)
- [ ] 4.23. Outdated name used for Permissions-Policy header
	- [x] Merge request
	- [ ] Document generator tool
- [ ] 4.24. Misconfigured Permissions-Policy-Header
	- [x] Merge request
- [ ] 4.25. Missing security attributes for links to foreign domains
	- [ ] \[**Dome?**\] Add noopener and noreferrer to links with tagret="\_blank"
- [ ] 4.26. Cookie consent not GDPR compliant
	- [ ] \[**Dome?**\]

## 4.1. Missing Updates (OpenSSH)

> [!danger] Severity
> high

Reply on our ticket \[TicketID: JAGF-0780-JVZQ\]:

- Die CVE-2019-16905 ist bei AlmaLinux Systemen nicht betroffen: https://access.redhat.com/security/cve/cve-2019-16905
- Die CVE-2020-15778 wird aus Kompatibilitätsgründen nicht behoben: https://access.redhat.com/security/cve/cve-2020-15778 (Hier sollte statt scp bspw. rsync verwendet werden)
	- "\[...\] In order to exploit this flaw, the attacker needs to social engineer or manipulate a system administrator (who has root access on the remote server) to run scp with a malicious command line parameter. \[...\]"
- Die CVE-2021-41617 wurde behoben: https://errata.almalinux.org/8/ALSA-2022-2013.html
- Die CVE-2016-20012 wird von Redhat als unkritisch eingestuft und aus dem Grund auch nicht gefixed: https://access.redhat.com/security/cve/cve-2016-20012
	- "\[...\] Although a CVE was assigned upstream and Red Hat doesn't consider it to be a security flaw and won't receive any patch, also the CVE was made as disputed by MITRE. Considering that Red Hat is closing this flaw as NOTABUG."

## 4.2. - 4.12.

> [!fail] Severity
> medium

## 4.2 Manipulatable email body

Todo \[Dome\] in application

## 4.3. X-Powered-By misconfiguration
Comment out
`Alias /icons/ "/usr/share/httpd/icons/"`
in /etc/httpd/conf.d/autoindex.conf

#### Result
Renders symfony 404 error 

## 4.4  Web server default components

See [[Topic/Security#4.3. X-Powered-By misconfiguration]]

#### Result
Renders symfony 404 error 

## 4.5. Unnecessary development files
Comment out 
`Alias /.noindex.html /usr/share/httpd/noindex/index.html`
in /etc/httpd/conf.d/welcome.conf

and add an Alias to Subscription "Hosting & DNS / Apache & nginx Settings" in `Additional directives for HTTP` and `Additional directives for HTTPS`

Production
`Alias /awstats/ /var/www/vhosts/cyberconference.schwarz/httpdocs/cyberconference.schwarz/public/index.php`
`Alias /cgi-bin/ /var/www/vhosts/cyberconference.schwarz/httpdocs/cyberconference.schwarz/public/index.php`

Staging
`Alias /awstats/ /var/www/vhosts/cyberconference.schwarz/httpdocs/staging.cyberconference.schwarz.zwei14.website/public/index.php`
`Alias /cgi-bin/ /var/www/vhosts/cyberconference.schwarz/httpdocs/staging.cyberconference.schwarz.zwei14.website/public/index.php`

and deactivate stats in the subscription.

### Result
Renders symfony 404 error

## 4.6. Exposure of sensitive services

**tbd.**

## 4.7. Missing Updates (PHP)

Siehe [[Topic/Security#4.5. Unnecessary development files]]

### Result
E.g. /icons/small/ renders symfony 404 error

## 4.8 Unvalidated redirect using an arbitrary Host-Header

## 4.9 Protocol to replace (TLS)

Add  (if file does not exist) ``/etc/httpd/conf.d/zzzz_ssl.conf

Insert:

```
# Insert
<IfModule mod_ssl.c>
SSLProtocol +TLSv1.2 +TLSv1.3
</IfModule>
```

## 4.10 Cipher to replace (TLS)

Add  (if file does not exist)  ``/etc/httpd/conf.d/zzzz_ssl.conf

Insert: Prefix all unsecure ciphers with a `!` 

### (A)

```
# after
<IfModule mod_ssl.c>
SSLCipherSuite EECDH+AESGCM+AES128:EECDH+AESGCM+AES256:EECDH+CHACHA20:EDH+AESGCM+AES128:EDH+AESGCM+AES256:EDH+CHACHA20:EECDH+SHA256+AES128:EECDH+SHA384+AES256:EDH+SHA256+AES128:EDH+SHA256+AES256:EECDH+SHA1+AES128:EECDH+SHA1+AES256:!EDH+SHA1+AES128:!EDH+SHA1+AES256:EECDH+HIGH:EDH+HIGH:AESGCM+AES128:AESGCM+AES256:CHACHA20:SHA256+AES128:SHA256+AES256:!SHA1+AES128:!SHA1+AES256:HIGH:!aNULL:!eNULL:!EXPORT:!DES:!RC4:!3DES:!MD5:!PSK:!KRB5:!aECDH:!kDH
SSLHonorCipherOrder on
</IfModule>

```


### (B)

```
<IfModule mod_ssl.c>
SSLProtocol +TLSv1.2 +TLSv1.3
</IfModule>
<IfModule mod_ssl.c>
#SSLCipherSuite EECDH+AESGCM+AES128:EECDH+AESGCM+AES256:EECDH+CHACHA20:EDH+AESGCM+AES128:EDH+AESGCM+AES256:EDH+CHACHA20:EECDH+SHA256+AES128:EECDH+SHA384+AES256:EDH+SHA256+AES128:EDH+SHA256+AES256:EECDH+SHA1+AES128:EECDH+SHA1+AES256:EDH+SHA1+AES128:EDH+SHA1+AES256:EECDH+HIGH:EDH+HIGH:AESGCM+AES128:AESGCM+AES256:CHACHA20:SHA256+AES128:SHA256+AES256:SHA1+AES128:SHA1+AES256:HIGH:!aNULL:!eNULL:!EXPORT:!DES:!RC4:!3DES:!MD5:!PSK:!KRB5:!aECDH:!kDH
##SSLCipherSuite EECDH+AESGCM+AES128:EECDH+AESGCM+AES256:EECDH+CHACHA20:EDH+AESGCM+AES128:EDH+AESGCM+AES256:EDH+CHACHA20:EECDH+SHA256+AES128:EECDH+SHA384+AES256:EDH+SHA256+AES128:EDH+SHA256+AES256:EECDH+SHA1+AES128:EECDH+SHA1+AES256:!EDH+SHA1+AES128:!EDH+SHA1+AES256:EECDH+HIGH:EDH+HIGH:AESGCM+AES128:AESGCM+AES256:CHACHA20:SHA256+AES128:SHA256+AES256:!SHA1+AES128:!SHA1+AES256:HIGH:!aNULL:!eNULL:!EXPORT:!DES:!RC4:!3DES:!MD5:!PSK:!KRB5:!aECDH:!kDH

SSLCipherSuite ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:DHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384:EECDH+SHA1+AES128:EECDH+SHA1+AES256:!SHA1+AES128:!SHA1+AES256

SSLHonorCipherOrder off
</IfModule>

```

> Do not disallow EECDH+SHA1+AES128 and EECDH+SHA1+AES256. Otherwise no letsencrypt certificates can be issued.

> Use https://ssl-config.mozilla.org/#server=apache&version=2.4.41&config=intermediate&openssl=1.1.1k&guideline=5.6 SSLCipherSuite config but add  the two mentioned above.

> https://www.ssllabs.com/ssltest/analyze.html?d=cyberconference.schwarz&hideResults=on


### Result (A)

```
$ nmap --script ssl-enum-ciphers -p 443 staging.cyberconference.schwarz.zwei14.website
Starting Nmap 7.93 ( https://nmap.org ) at 2022-11-23 09:56 CET
Nmap scan report for staging.cyberconference.schwarz.zwei14.website (46.4.77.133)
Host is up (0.033s latency).
rDNS record for 46.4.77.133: srv426504.dogado-shops.de

PORT    STATE SERVICE
443/tcp open  https
| ssl-enum-ciphers: 
|   TLSv1.2: 
|     ciphers: 
|       TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256 (ecdh_x25519) - A
|       TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384 (ecdh_x25519) - A
|       TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305_SHA256 (ecdh_x25519) - A
|       TLS_RSA_WITH_AES_128_GCM_SHA256 (rsa 2048) - A
|       TLS_RSA_WITH_AES_256_GCM_SHA384 (rsa 2048) - A
|     compressors: 
|       NULL
|     cipher preference: server
|   TLSv1.3: 
|     ciphers: 
|       TLS_AKE_WITH_AES_128_GCM_SHA256 (ecdh_x25519) - A
|       TLS_AKE_WITH_AES_256_GCM_SHA384 (ecdh_x25519) - A
|       TLS_AKE_WITH_CHACHA20_POLY1305_SHA256 (ecdh_x25519) - A
|     cipher preference: client
|_  least strength: A

Nmap done: 1 IP address (1 host up) scanned in 2.50 seconds
```

```
$ sslscan --show-ciphers staging.cyberconference.schwarz.zwei14.website:443
[...]
Heartbleed:
TLSv1.3 not vulnerable to heartbleed
TLSv1.2 not vulnerable to heartbleed

  Supported Server Cipher(s):
Preferred TLSv1.3  128 bits  TLS_AES_128_GCM_SHA256        Curve 25519 DHE 253
Accepted  TLSv1.3  256 bits  TLS_AES_256_GCM_SHA384        Curve 25519 DHE 253
Accepted  TLSv1.3  256 bits  TLS_CHACHA20_POLY1305_SHA256  Curve 25519 DHE 253
Preferred TLSv1.2  128 bits  ECDHE-RSA-AES128-GCM-SHA256   Curve 25519 DHE 253
Accepted  TLSv1.2  256 bits  ECDHE-RSA-AES256-GCM-SHA384   Curve 25519 DHE 253
Accepted  TLSv1.2  256 bits  ECDHE-RSA-CHACHA20-POLY1305   Curve 25519 DHE 253
Accepted  TLSv1.2  128 bits  AES128-GCM-SHA256            
Accepted  TLSv1.2  256 bits  AES256-GCM-SHA384      
```

### Result (B) 

better

```
$ nmap --script ssl-enum-ciphers -p 443 staging.cyberconference.schwarz.zwei14.website
Starting Nmap 7.93 ( https://nmap.org ) at 2022-12-09 16:43 CET
Nmap scan report for staging.cyberconference.schwarz.zwei14.website (46.4.77.133)
Host is up (0.031s latency).
rDNS record for 46.4.77.133: srv426504.dogado-shops.de

PORT    STATE SERVICE
443/tcp open  https
| ssl-enum-ciphers: 
|   TLSv1.2: 
|     ciphers: 
|       TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256 (ecdh_x25519) - A
|       TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384 (ecdh_x25519) - A
|       TLS_ECDHE_RSA_WITH_CHACHA20_POLY1305_SHA256 (ecdh_x25519) - A
|     compressors: 
|       NULL
|     cipher preference: server
|   TLSv1.3: 
|     ciphers: 
|       TLS_AKE_WITH_AES_128_GCM_SHA256 (ecdh_x25519) - A
|       TLS_AKE_WITH_AES_256_GCM_SHA384 (ecdh_x25519) - A
|       TLS_AKE_WITH_CHACHA20_POLY1305_SHA256 (ecdh_x25519) - A
|     cipher preference: client
|_  least strength: A

```

```
$ sslscan --show-ciphers cyberconference.schwarz:443
[...]
  Heartbleed:
TLSv1.3 not vulnerable to heartbleed
TLSv1.2 not vulnerable to heartbleed

  Supported Server Cipher(s):
Preferred TLSv1.3  128 bits  TLS_AES_128_GCM_SHA256        Curve 25519 DHE 253
Accepted  TLSv1.3  256 bits  TLS_AES_256_GCM_SHA384        Curve 25519 DHE 253
Accepted  TLSv1.3  256 bits  TLS_CHACHA20_POLY1305_SHA256  Curve 25519 DHE 253
Preferred TLSv1.2  128 bits  ECDHE-RSA-AES128-GCM-SHA256   Curve 25519 DHE 253
Accepted  TLSv1.2  256 bits  ECDHE-RSA-AES256-GCM-SHA384   Curve 25519 DHE 253
Accepted  TLSv1.2  256 bits  ECDHE-RSA-CHACHA20-POLY1305   Curve 25519 DHE 253
```

## 4.11. Weak SSH algorithm

> [!info] Severity
> https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/8/html/security_hardening/using-the-system-wide-cryptographic-policies_security-hardening
> https://www.redhat.com/en/blog/how-customize-crypto-policies-rhel-82
> `update-crypto-policies --show`
> `update-crypto-policies --set DEFAULT`


Test configuration with:
`ssh-audit staging.cyberconference.schwarz.zwei14.website`

### /etc/ssh/sshd_config

*Maybe not worth it. Seems to do nothing at all.*

```
# Add line if not present
KexAlgorithms curve25519-sha256,curve25519-sha256@libssh.org,diffie-hellman-group-exchange-sha256,diffie-hellman-group16-sha512,diffie-hellman-group18-sha512,diffie-hellman-group14-sha256
```

### /etc/crypto-policies/back-ends/openssh.config

Remove all  `key exchange algorithms` entries in ssh-audit marked with `[fail]` and `[warn]` from `/etc/crypto-policies/back-ends/openssh.config` from Line starting with `KexAlgorithms`.

Remove all  `encryption algorithms (ciphers)` entries in ssh-audit marked with `[fail]` and `[warn]` from `/etc/crypto-policies/back-ends/openssh.config` from Line starting with `Ciphers`.

Remove all  `message authentication code algorithms` entries in ssh-audit marked with `[fail]` and `[warn]` from `/etc/crypto-policies/back-ends/openssh.config` from Line starting with `MACs`.

### /etc/crypto-policies/back-ends/opensshserver.config

Remove all  `key exchange algorithms` entries in ssh-audit marked with `[fail]` and `[warn]` from `/etc/crypto-policies/back-ends/opensshserver.config` from `-oKexAlgorithms` arguments.

Remove all  `host-key algorithms` entries in ssh-audit marked with `[fail]` and `[warn]` from `/etc/crypto-policies/back-ends/opensshserver.config` from `-oHostKeyAlgorithms` arguments.

Remove all  `encryption algorithms (ciphers)` entries in ssh-audit marked with `[fail]` and `[warn]` from `/etc/crypto-policies/back-ends/opensshserver.config` from `-oCiphers arguments.

Remove all  `message authentication code algorithms` entries in ssh-audit marked with `[fail]` and `[warn]` from `/etc/crypto-policies/back-ends/opensshserver.config` from `-oMACs arguments.

### Restart sshd service

**IMPORTANT** Run `sshd -t` before restarting the sshd service. 
**Restart only if no errors occur**.

Restart e.g. with `systemctl restart sshd.service` .

`sshd -T` will show the current config values.

### Result

![[ssh-audit.png]]

## 4.12. Outdated SSH algorithm

See [[Topic/Security#4.11. Weak SSH algorithm]]

## 4.13. - 4.15.

> [!warning] Severity
> low

## 4.13. Missing Expect-CT-Header

NOTABUG

![[expect-ct.png]]
https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Expect-CT

## 4.14. Insecure Content-Security-Policy

> [!info] Recommendation
> For initial tests with a new CSP, use the Content-Security-Policy-Report-Only configuration to monitor the possible effects of the given configuration. **Avoid using settings like unsafe-inline if possible** as they disable protection from cross-site-scripting attacks.

Merge request [https://gitlab.com/zwei14/schwarz-gruppe/cyberconference.schwarz/symfony-app/-/merge_requests/1] in place.

## 4.15. Missing OCSP stapling

**tbd.**
https://support.plesk.com/hc/en-us/articles/115001733665-Is-it-possible-to-enable-OCSP-Stapling-for-a-domain-in-Plesk-

Add `lsocsp.conf` to `/etc/httpd/conf.d`

Insert 

```
<IfModule Litespeed>
    SSLStaplingCache shmcb:/var/run/ocsp(128000)
    SSLUseStapling on
</IfModule>
```

## from 4.16. 

> [!info] Severity
> none

## 4.16. DKIM not used for emails

Should be fixed in application already.

## 4.17. Cross-domain script inclusion

**tbd**

## 4.18. Referrer misconfiguration

**tbd**

## 4.19. Insecure Access-Control-Allow-Origin policy

Use Access-Control-Allow-Origin "same-origin" for file extensions ttf|ttc|otf|eot|woff|woff2|font.css|css|js|gif|png|jpe?g|svg|svgz|ico|webp

Merge request [https://gitlab.com/zwei14/schwarz-gruppe/cyberconference.schwarz/symfony-app/-/merge_requests/2] is in place.

## 4.20. Using SSH with password-based authentication

Change in `/etc/ssh/sshd_config`

```
PasswordAuthentication no
Match Address <static ip>
	PasswordAuthentication yes
```

## 4.21. Using the default port for SSH

**tbd**

## 4.22. Business-logic filtering on the client

**tbd**

## 4.23. Outdated name used for Permissions-Policy header

Merge request in place: https://gitlab.com/zwei14/schwarz-gruppe/cyberconference.schwarz/symfony-app/-/merge_requests/3

## 4.24. Misconfigured Permissions-Policy-Header

See [[Topic/Security#4.23. Outdated name used for Permissions-Policy header]]

## 4.25. Missing security attributes for links to foreign domains

Todo \[Dome\] in application: Add noopener and noreferrer to links with tagret="\_blank"

## 4.26. Cookie consent not GDPR compliant

Todo \[Dome\]

## Some soures
https://serverfault.com/questions/758673/how-to-disable-diffie-hellman-group1-sha1-for-ssh
