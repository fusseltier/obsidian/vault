## Leistungsbeschreibung Dynamischer Gastarif 
Grundsätzlich nehmen wir eine Adaption und Erweiterung der Implementierung für den bestehenden dynamischen Stromtarif vor.

## Todos

- Kopie und Erweiterung des bestehenden **Content Elements**, in dem der Pfad zu den Dateien, sowie beispielsweise die Fußnoten gepflegt werden können
	- Erweiterung um ein Select Feld für die Stufen 1 - 6 (bis 1000, 1001 bis 4000, etc.) oder ein Textfeld
	- Erweiterung des Balkendiagramms: Farbige Darstellung der drei Preisbestandteile im jeweiligen Balken
- Kopie und Erweiterung des **ViewHelpers**. Dieser erzeugt das zur Darstellung notwendige HTML, CSS und JavaScript
	- Erweiterung um die Verarbeitung der drei Preisbestandteile und die optische Ausgabe des Select Felds für die Stufen 1 - 6 oder des Textfelds
- Ermittlung des Gesamtpreises (Addition der Bestandteile) und Rundung auf zwei Nachkommastellen
- Einbindung und initiale Pflege für die letzten 24 Monate

### Aufbau der Dateien

- Für jeden Monat wird eine **Textdatei** angelegt. Die Benennung erfolgt analog zu den Dateien für den Stromtarif (`JJJJ-MM.txt`).
- Die Datei ist wie folgt aufgebaut:
	- Für jede Stufe enthält die Datei eine Zeile:
		-  `min|max|Netto Preisbestandteil 1|NettoPreisbestandteil 2|NettoPreisbestandteil 3` 
		- also bspw.
			- Zeile 1: 0|1000|aa.bbb|cc.ddd|ee.fff
			- Zeile 2: 1001|4000|aa.bbb|cc.ddd|ee.fff
			- Zeile 3: 4001|...

## Aufwand

ca. 1 - 2 Tage 

## Inputs

Wir benötigen:
- Texte Kleingedrucktes 1-3
	- 1: Y-Achse
	- 2: X-Achse
	- 3: Durchsnittspreis
-  3 Preisbestandteile für alle Stufen für die letzten 24 Monate
	- Idealerweise in einzelnen txt-Dateien. Eine Excel Datei wäre auch ok.
		- Eine Definition, welcher Preis welcher Bestandteil ist, am besten so sortiert wie wir die Bestandteile im Balken übereinander darstellen sollen
			- Netto Preisbestandteil 1: Unten
			- Netto Preisbestandteil 2: Mitte
			- Netto Preisbestandteil 3: Oben
		- Für jeden Monat wird eine **Textdatei** angelegt. Die Benennung erfolgt analog zu den Dateien für den Stromtarif (`JJJJ-MM.txt`).
			- Die Datei ist wie folgt aufgebaut:
				- Für jede Stufe enthält die Datei eine Zeile:
					-  `Preisstufe Min.|Preisstufe Max.|Netto  Preisbestandteil 1|Netto Preisbestandteil 2|Netto Preisbestandteil 3` 
					- also bspw. eine Datei: `2022-12.txt`
						- Zeile 1: 0|1000|aa.bbb|cc.ddd|ee.fff
						- Zeile 2: 1001|4000|aa.bbb|cc.ddd|ee.fff
						- Zeile 3: 4001|...
						- ...