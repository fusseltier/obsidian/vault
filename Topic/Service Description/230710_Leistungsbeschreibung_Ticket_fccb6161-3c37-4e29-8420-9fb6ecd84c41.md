#Leistungsbeschreibung #fccb6161-3c37-4e29-8420-9fb6ecd84c41

## Anforderung:  

In der Visualisierung des Inkubators inkl. der Zubehöre, die in das Gerät passen, soll es möglich sein Abstände zwischen den Zubehören zu visualisieren, die durch verschieden hohe Gegenstände auf den Zubehören notwendig werden.

## Lösungsansatz:

Aktuell werden die Zubehöre einer Konfiguration (Objekt: HettichConfiguration) jeweils zusammengefasst gespeichert. Das heißt in der Konfiguration gibt es aktuell für jedes hinzugefügte Zubehör einen Eintrag, in dem die folgenden Werte gespeichert werden:

- Min. (Minimale Anzahl in der Konfiguration)
- Max. (Maximale Anzahl in der Konfiguration)
- Count (Konfigurierte Anzahl in der Konfiguration)
- Internal Count (im Gerät)
- Additional Count (zusätzlich)

Im Prinzip kann man sich das wie eine Tabelle vorstellen:

	+------+------+-------+----------------+------------------+
	| Min. | Max. | Count | Internal Count | Additional Count |
	+------+------+-------+----------------+------------------+
	| 0    | 30   | 3     | 3              | 0                |
	+------+------+-------+----------------+------------------+

Diese Art der Speicherung soll beibehalten werden, da eine Änderung der Speicherung jedes Zubehörs mit einem eigenen Eintrag in der Konfiguration massive Änderungen in der gesamten Logik der Applikation nach sich ziehen würde bspw. beim Hinzufügen, beim Entfernen oder bei der Ermittlung von nicht entfernbaren Zubehören.

Daher werden wir im folgenden einen möglichen **Lösungsansatz** zur Erweiterung der bestehenden Applikation **unter Beibehaltung der zentralen Logik** beschreiben.

Folgende Objekte müssen angepasst werden:

- HettichConfiguration

Das Objekt wird hierbei um ein weiteres Feld für die Metadaten erweitert. In diesem Feld wird eine kommseparierte Liste gespeichert, die die Anzahl an Slots über dem Zubehör repräsentieren. Diese Liste kann für die Berechnung und die Visualisierung ausgewertet werden.

Das Objekt wird also erweitert um das Feld

- Slots above (komma separierte Liste)

Wurde also beispielsweise 3x das Zubehör 60024 hinzugefügt, sieht die Liste initial wie folgt aus:

- 0,0,0

Jeder Eintrag in der Liste repräsentiert die Slots oberhalb des Zubehörs, die nicht belegt werden sollen. Konfiguriert der Nutzer bspw., dass über dem untersten der drei hinzugefügten Zubehöre 60024 XX cm frei bleiben sollen (wobei XX umgerechnet in unserem Beispiel 3 Slots entspricht), so wird in der Liste der folgende Eintrag gespeichert:

- 3,0,0

Die "Tabelle" sieht dann so aus:

	+------+------+-------+----------------+------------------+-------------+
	| Min. | Max. | Count | Internal Count | Additional Count | Slots above |
	+------+------+-------+----------------+------------------+-------------+
	| 0    | 30   | 3     | 3              | 0                | 3,0,0       |
	+------+------+-------+----------------+------------------+-------------+

<div style="page-break-after:always"></div>

## Folgende Funktionen müssen (mindestens) erweitert werden

### updateConfigurationAccessoriesMetadata([...])

Diese Funktion berechnet die Anzahl der Zubehöre, die in den Inkubator passen (Internal, sowie Additional Count). Diese Funktion muss erweitert werden, sodass die Abstände oberhalb der Zubehöre bei der Berechnung berücksichtigt werden.

### getBookedAccessoriesVisualHtml([...])

Diese Funktion generiert die visuelle Ausgabe der Zubehöre im Inkubator. Die Funktion muss erweitert werden, sodass die Abstände oberhalb der Zubehöre in der Visualisierung berücksichtigt werden.

### getBookedAccessoriesInternalListHtml([...])

Diese Funktion generiert das HTML der Liste der Zubehöre im Gerät. Die Ausgabe muss um die Möglichkeit erweitert werden über grafische Elemente Abstände über einzelnen Zubehören zu konfigurieren. Hier könnte beispielsweise ein aufklappbares Element unter jedem Zubehör integriert werden, über das sich Abstände bspw. über ein Drop-Down definieren lassen.

Im Dokument `salestools_hettcube_loading_equipment.pdf` ist der Abstand der Positionen (Einhänge) Unterkante zu Unterkante mit 43mm angegeben. Das habe ich als grobe Referenz in der Sidebar für die Darstellung herangezogen, sodass dort 1 Slot (eine Position) ca 43mm entspricht.

	+-------------------------------------+
	| 60024 | +-----------------------+
	|       | | bis  4,3 cm (1 Slot)  |
	|       | | bis  8,6 cm (2 Slots) |
	|       | | ...                   |
	| 60024 | +-----------------------+
	|       | | bis  4,3 cm (1 Slot)  |
	|       | | bis  8,6 cm (2 Slots) |
	|       | | ...                   |
	+-------------------------------------+
	...

Die einzelnen Zubehöre müssen in dem Zuge dann von unten ausgerichtet werden, sodass Anstände immer nach oben dargestellt werden.
## Weiteres Vorgehen

Sofern der dargelegte Vorschlag den Erwartungen entspricht, werden wir zunächst testweise die Anpassungen am Objekt **HettichConfiguration** sowie den für die **Berechnung** und **Visualisierung im Inkubator** notwendigen Anpassungen vornehmen und diese testen. Funktioniert der Ansatz, wie geplant, kann die beschriebene Erweiterung der Liste erfolgen, sodass Nutzer das Feature nutzen können.