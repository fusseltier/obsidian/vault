#service-description #svs

**Variante 1**

>Die Berechnung soll automatisiert („intelligent“) und tagesweise über eine Datei erfolgen, in der die stündlichen Energiepreise erfasst werden (liegt uns vor). Diese werden mit einem Standard-Lastprofil (angenommenes Verbrauchsprofil/Stunde – liegt uns vor) kombiniert und dann ausgegeben werden. Somit ändert sich jeden Tag der aktuelle Monatspreis.\*

\* Siehe Mail vom 05. Dez. 2022 
  **Betreff:** AW: Visualisierung Website neues Stromprodukt

Unsere Leistungsbeschreibung bezieht sich auf **Variante 1**. 

## Rückfragen

- Wie sieht die Datei am 01.01.23 aus? Ich gehe davon aus, dass sie in Sheet **Spot_PWR**, Spalte **B** Zeile **21** mit dem Preis für 01/01/2023 startet?
  >ja
- Wieso unterscheiden sich die Uhrzeiten für 01/01/2022 (0:00, 1:00 - 23:00) von den Uhrzeiten aller anderen Tage (0:00, 0:59 - 22:59)? Wird das 2023 wieder so sein?
  >das ist bei mir nicht so….muss an der Darstellung in Excel liegen…
- Preise können positiv sowie negativ sein?
  >a in den einzelnen Stunden schon – in der Aggregation auf einen Monat eher nein. Das wäre sehr ungewöhnlich.
- Die Datei soll einmal täglich aktuell zur Verfügung gestellt werden?
  >ja
- Wie werden die Endkundenpreise für jeden Monat ermittelt, bzw. ZWEI14 zur Verfügung gestellt?
  >Die Endkundenpreise sollen automatisch durch eine Formel berechnet werden. Hier kommt dann das Standard-Lastprofil hinzu.

## Grundsätzliches Vorgehen:

Wir implementieren eine visuelle Darstellung der Preisentwicklung über standardmäßig insgesamt 12 Monate. 

Diese umfasst beim Öffnen der Seite jeweils die 12 aktuellsten Monate, für die über eine

- (A) stundenaktuellen Excel Datei (syneco), **gespeichert ohne Makros**
- (B) einer von SVS aufbereitete stundenaktuelle CSV Datei

Daten zum Strompreis an der Börse, sowie den Endkundenpreis der SVS vorliegen. 

Die Visualisierung erfolgt über ein Balkendiagramm. Für jeden Monat werden zwei Balken dargestellt (Strompreis an der Börse (ct/kWh), Endkundenpreis der SVS (ct/kWh)). Die Skala wird in 10 Cent Schritten aufgebaut von 0 - "x" (vgl. Beispiel EGT). Die Skala (bzw. "x" richtet sich nach dem höchsten Preis innerhalb der dargestellten Monate.

Zusätlich wird der Durchschnittspreis über die dargestellten Monate mit einer waagerechten Linie ausgegeben.

Die Darstellung erfolgt voraussichtlich über ein neues TYPO3 Content-Element.

### Zeiteräume

Der Nutzer kann Über ein Drop-Down Menü verschiedene Zeiträume auswählen, für die die Preisentwicklung visuell dargestellt werdern soll:

- 6 Monate
- 12 Monate
- 18 Monate
- 24 Monate
- etc.

**Die möglichen Zeiträume müssen vorab abgestimmt werden. Diese sind außerdem anhängig von den vorliegenden Daten. Aktuell wären beispielsweise Daten für insgesamt 12 Monate vorhanden.**

### A. Die Datei(en) sind wie folgt aufgebaut:

Es gibt eine Datei für jedes Jahr - benannt "YYYY.xlsx" (also bspw. 2022.xlsx, 2023.xlsx). Die Datei wird **ohne Makros** gespeichert und zur Verfügung gestellt.

Jede Datei enthält ein Sheet **"Spot_PWR"**. Dieses beinhaltet stundenaktuelle Daten.

![[Pasted image 20221214100503.png]]

Relevant sind die Inhalte der Spalten **B** und **C** ab Zeile 21. Zeile 20 enthält jeweils die Überschrift.

- **Spalte B, Zeile 20**
	- Enthält die Überschrift "Datum"
- **Spalte C, Zeile 20**
	- Enthält die Überschrift "EUR/MWh"
- **Spalte B, Zeile 21 - n**
	- Enthält Datum und Uhrzeit mit einem Leerzeichen getrennt
		- Das Datum liegt im Format j/n/Y vor
			- `j` - Day of the month without leading zeros -  `1` to `31`
			- `n` - Numeric representation of a month, without leading zeros -  `1` through `12`
			- `Y` - A full numeric representation of a year, at least 4 digits, with - for years BCE. Examples: `-0055`, `0787`, `1999`, `2003`, `10191`
		- Die Uhrzeit liegt im Format G:i beziehungsweise
			- `G` - 24-hour format of an hour without leading zeros - `0` through `23`
			- `i` - Minutes with leading zeros - `00` to `59`
- **Spalte C, Zeile 21 - n**
	- Enthält den Preis
		- Dieser Kann als Float (bspw. +/-134.13) und Ganzzahl (bspw. +/-262) vorliegen

Unterhalb der Zeile **n** sind die Felder leer.

Die Dateien werden in einem von ZWEI14 bereitgestellten Ordner hochgeladen und ausgelesen.

### B.  Die Datei(en) sind wie folgt aufgebaut:

Es gibt eine Datei für jedes Jahr - benannt "YYYY.csv" (also bspw. 2022.xlsx, 2023.csv). Die Datei aus **(A)** dient als Basis. Die relevanten Daten werden in eine CSV Datei überführt und zur Verfügung gestellt.

- **Spalte 1, Zeile 1**
	- Enthält die Überschrift "Datum"
- **Spalte 2, Zeile 1**
	- Enthält die Überschrift "EUR/MWh"
- **Spalte 1, Zeile 2 - n**
	- Enthält Datum und Uhrzeit mit einem Leerzeichen getrennt
		- Das Datum liegt im Format j/n/Y vor
			- `j` - Day of the month without leading zeros -  `1` to `31`
			- `n` - Numeric representation of a month, without leading zeros -  `1` through `12`
			- `Y` - A full numeric representation of a year, at least 4 digits, with - for years BCE. Examples: `-0055`, `0787`, `1999`, `2003`, `10191`
		- Die Uhrzeit liegt im Format G:i beziehungsweise
			- `G` - 24-hour format of an hour without leading zeros - `0` through `23`
			- `i` - Minutes with leading zeros - `00` to `59`
- **Spalte 2, Zeile 2 - n**
	- Enthält den Preis
		- Dieser Kann als Float (bspw. +/-134.13) und Ganzzahl (bspw. +/-262) vorliegen

Unterhalb der Zeile **n** sind die Felder leer.

Wie aus dem Aufbau ersichtlich, können die Daten bspw. aus Excel kopiert und in einer leeren Datei eingefügt werden. Diese muss dann als CSV Datei exportiert werden.

Die Dateien werden in einem von ZWEI14 bereitgestellten Ordner hochgeladen und ausgelesen.

### Endkundenpreise

Der Endkundenpreis wird für jeden Monat aus dem jeweils berechneten Durchschnits Spot Preis des Monats anhand einer Formel berechnet, die SVS zur Verfügung stellt.

### Berechnung des visuell ausgegebenen Spot Preises

Der Spot-Preis in der visuellen Darstellung ist jeweils der Durchschnittspreis für alle vorliegenden Daten eines Monats. Dieser muss von ZWEI14 ermittelt werden.

> [!info] Anmerkung
> Aktuell geht ZWEI14 davon aus, dass der im Konfigurator angezeigte Preis einmal zum Monatswechsel von SVS aktualisiert wird. Dieser entspricht dem ermitteltem Preis vom Vormonat. Es ist also möglich, dass der in der Visualisierung ausgegebene Endkundenpreis vom im Konfigurator angezeigten Preis abweicht, wenn nicht nur volle (bereits abgelaufene) Monate ausgegeben werden, da der tagesaktuelle Durchschnittspreis angezeigt wird. 


## Platzierung

Die Visualisierung wird zunächst auf einer dafür neu angelegten Contentseite eingebunden.

*In einem zweiten Schritt kann diese auch auf der jeweiligen Detailseite des neuen Stromtarifs ausgegeben werden. Dies setzt allerdings ein noch zu definierendes Attribut im CSIT Tarif voraus, über das die Anzeige gesteuert werden kann.* **Dieser zweite Schritt ist im geschätzten Aufwand nicht berücksichtigt.**

## Vergleich mit Variante 2

Im Vergleich zu Variante 2 ist die Umsetzung für Variante 1.A. deutlich aufwendiger. Die Verarbeitung eines einzelnen Sheets einer Excel Datei ist in PHP nur mit externen Libraries möglich. Das macht die Verarbeitung schwieriger. Variante 1.B. ist im Vergleich zur Variante 1.A. durch die Nutzung von CSV Dateien weniger Aufwendig.

Insgesamt gilt: **Aufwand 1.A. > 1.B. > 2.**

Zudem müssen im Vergleich zu Variante 2 mehr Berechnungen von ZWEI14 durchgeführt werden. So muss der angezeigte monatliche Spot Preis jeweils über alle Tage/Stunden eines Monats ermittelt werden. 
