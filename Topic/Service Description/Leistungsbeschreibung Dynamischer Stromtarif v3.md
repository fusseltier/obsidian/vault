#service-description #svs

**Variante 3**

>Alternativ: Bitte auch anbieten – wir berechnen die Preise jeden Monat und lassen den Wert eintragen oder können diesen im Chart selbst eintragen. Zwei 14 programmiert lediglich das Chart, der Wert wird von SVS gebildet.
>Dann muss keine Datei verarbeitet werden. Die Aktualisierung erfolgt nur monatlich und nicht täglich.\*

\* Siehe Mail vom 14. Dez. 2022 
  **Betreff:** AW: Visualisierung Website neues Stromprodukt

Unsere Leistungsbeschreibung bezieht sich auf **Variante 3**. 

## Grundsätzliches Vorgehen:

Wir implementieren eine visuelle Darstellung der Preisentwicklung über standardmäßig insgesamt 12 Monate. 

Diese umfasst beim Öffnen der Seite jeweils die 12 aktuellsten Monate, für die Daten zum Strompreis an der Börse, sowie den Endkundenpreis der SVS vorliegen. 

Die Visualisierung erfolgt über ein Balkendiagramm. Für jeden Monat werden zwei Balken dargestellt (Strompreis an der Börse (ct/kWh), Endkundenpreis der SVS (ct/kWh)). Die Skala wird in 10 Cent Schritten aufgebaut von 0 - "x" (vgl. Beispiel EGT). Die Skala (bzw. "x" richtet sich nach dem höchsten Preis innerhalb der dargestellten Monate.

Zusätzlich wird der Durchschnittspreis über die dargestellten Monate mit einer waagerechten Linie ausgegeben.

Die Darstellung erfolgt über ein neues TYPO3 Content-Element. Alle Preise werden im Contentelement von SVS (einmal im Monat) gepflegt.

### Zeiteräume

Der Nutzer kann Über ein Drop-Down Menü verschiedene Zeiträume auswählen, für die die Preisentwicklung visuell dargestellt werdern soll:

- 12 Monate
- 18 Monate
- 24 Monate
- etc.

**Die möglichen Zeiträume müssen vorab abgestimmt werden. Diese sind außerdem anhängig von den vorliegenden Daten.**

### Pflege der Daten:

Im Contentelement werden Textfelder für die Jahre 2022 - 2025 bereitgestellt. 

Für jeden Monat gibt es zwei Felder 

- Feld 1: Strompreis an der Börse (ct/kWh)
	- Der Preis wir als float Value angegeben (also z.B: 23.323)
- Feld 2: Strompreis der SVS (ct/kWh)
	- Der Preis wir als float Value angegeben (also z.B: 23.323)

Liegen (noch) keine Daten für einen Monat vor, so bleiben die Felder 1 und 2 leer.

## Platzierung

Die Visualisierung wird zunächst auf einer dafür neu angelegten Contentseite eingebunden.

*In einem zweiten Schritt kann diese auch auf der jeweiligen Detailseite des neuen Stromtarifs ausgegeben werden. Dies setzt allerdings ein noch zu definierendes Attribut im CSIT Tarif voraus, über das die Anzeige gesteuert werden kann.* **Dieser zweite Schritt ist im geschätzten Aufwand nicht berücksichtigt.**