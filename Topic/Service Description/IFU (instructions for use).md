#service-description #hsw

## Anforderung

HSW muss als Hersteller von Medizinprodukte den Download einer Gebrauchsanweisung (IFU) für Ihre Produkte auf der Webseite ermöglichen.

## Implementierung

### Backend

Die Dateien werden als PDF in der Dateiliste (im TYPO3 Backend) hochgeladen. Der Ordner wird "ifu" benannt. Der Pfad (ausgehend vom Document Root) wird "/fileadmin/user_upload/ifu/" lauten. Die Dokumente sind mehrsprachig angelegt.

Die Tabelle "sys_file_metadata" wird um ein Textfeld  "article_numbers" erweitert. In diesem können die Artikelnummern der Gebrauchsanweisung kommasepariert gespeichert werden. 
Im Backend wird das Feld im Reiter "Metataden" der jeweiligen Datei mit dem Label "Article number(s)"  ausgegeben. Dort können die Artikelnummern kommasepariert eingetragen werden.

### Frontend

Im Frontend wird eine neue Seite "IFU" unterhalb der Seite "Downloads" angelegt. Auf dieser Seite wird ein Suchfeld mit einem Button platziert. In das Suchfeld kann der Nutzer eine Artikelnummer eintragen und über den Button die Suche anstoßen. Die Artikelnummer muss komplett eingegeben werden. Eine automatische Vervollständigung im Suchfeld wird bewusst nicht implementiert.

Weitere Elemente, wie beispielsweise eine Headline, erklärende Texte und andere Schmuckelemente werden im Layout definiert.

Das Formular wird voraussichtlich als Content Element in der Extension ¨kitt3n/kitt3n-custom" implementiert.\[\*\]

Im Hintergrund werden dann über eine Datenbankabfrage alle Dokumente, die die Artikelnummer im Feld "article_numbers" enthalten gesucht. Gibt es einen (oder mehrere) Treffer, werden diese unterhalb des Suchfelds ausgegeben. 

Die Suchlogik wird voraussichtlich über einen DataProcessor implementiert.(\[\*\]

Angezeigt werden entweder der Dateiname oder (besser) das Feld "title" aus der Tabelle "sys_file_metadata", falls dieses im Backend befüllt wurde.

Der ausgegeben Titel ist ein Link. Der Nutzer kann durch einen Klick auf den Link das Dokument herunterladen.

#### Download

Für den Download gibt es zwei Optionen:

1. Der Link zur Datei ist der Pfad zum Dokument, also bzpw. /fileadmin/user_upload/ifu/\<Gebrauchsanweisung-XYZ.pdf\>. Je nachdem wie der Nutzer seinen Browser konfiguriert hat, wird das Dokument entweder heruntergeladen oder in einem vom Browser konfigurierten Programm (z.B. Adobe Acrobat, oder dem im Browser integrierten PDF-Betrachter) aufgerufen.
2. Der Link zum Dokument ist nicht der Pfad zum Dokument. Vielmehr ruft der Link ein Script auf (bspw. /download_ifu.php?ifu=\<Eindeutiger Identifier des Dokuments>), das direkt den Download anstößt. Das Dokument wird dann immer direkt heruntergeladen.

#### Texte

Alle feststehenden Texte wie Labels im Content Element (Headline, Suchfeld, Button, etc.) werden über XLIFF Dateien jeweils in englisch und deutsch bereitgestellt. "XLIFF (XML Localization Interchange File Format) is an XML-based bitext format created to standardize the way localizable data are passed between and among tools during a localization "Internationalization and localization") process \[...\]".

Die Texte müssen im Vorfeld abgestimmt werden und in englischer und deutscher Sprache vorliegen.

### Aufwand

Für die Implementierung ensteht ein Aufwand von **ca. 1,5 - 2,5 Tagen**.

--

\[\*\] Die tatsächliche Implementierung kann von der Beschreibung abweichen. An der zur verfügung gestellten Funktionalität für die Nutzer, die das Dokument herunterladen und für die Pflege im TYPO3 Backend hat dies keine Auswirkungen.   
