#Leistungsbeschreibung
## Anforderung
### A
Logout im Intranet (aus Pimcore) soll einen Logout in der Azure Applikation triggern.

### B
Der Logout soll nach einer Zeit x der Inaktivität automatisch erfolgen.

## Lösungsansätze
### A
Anforderung **A** lässt sich **nur partiell realisieren**.So zumindest unser aktueller Kennstnisstand zu den Möglicheiten, die die entprechende API bietet.

Der Login erfolgt über die Microsoft Graph API via oAuth. Für den Logout gibt es einen definierten Endpunkt https://login.microsoftonline.com/<tenant\>/oauth2/v2.0/logout.

In der Controller Action, die den Logout handelt können wir auf diesen weiterleiten. Sofern der User allerdings in mehreren Azure Konten / Applikationen angemeldet ist, muss er über einen Dialog aktiv auswählen, welchen Nutzer er abmelden möchte. Ansonsten bleibt der Login in Azure aktiv, bis er dort abläuft. Auf die Dauer hat ZWEI14 keinen direkten Einfluss. Aktuell dürfte das eine Stunde sein, sofern die von Herr Benkert definierte **Conditional Access policy** greift. ZWEI14 kann nicht beeinflussen, ob der Nutzer den Logout aktiv in einem Dialog per Klick bestätigen muss oder nicht. Wir können lediglich auf den Endpunkt weiterleiten.

### B
Aktuell werden beim Login zum einen verschiedene Informationen aus dem Azuer AccessToken verrschlüsselt in Cookies gespeichert. Diese Cookies laufen mit dem AccessToken nach 60-90 Minuten ab. Der AccessToken wird auch in einer Session Variable gespeichert, die beim Schließen des Browsers gelöscht wird.

Daneben wird ein User generiert, der in Pimcore angemeldet wird. Hier wird aktuell die Browser Session genutzt. Das heißt nach Schließen des Browsers, muss man sich erneut anmelden.

Bei jedem Seitenaufruf wird zudem im Hintegrund geprüft, ob der AccessToken in der Session bereits abgelaufen ist und dieser sowie die Cookies dann ggf. erneuert. Dies geschieht vor allem vor dem Hintergrund, dass sich beispielsweise die Berechtigungen (Gruppen) eines Users in der Zwuschenzeit geändert haben könnten.

### Wir können nun folgendes implementieren

Über eine Konfiguration kann die Zeit in Sekunden definiert werden, nach der der User bei Inaktivität automatisch aus dem Intranet abgemeldet werden soll. Dafür wird beim Login ein weiterer Cookie gesetzt, der für die angegebene Zeitspanne gültig ist.

Über einen EventListener kann bei jedem Seitenaufruf im Intranet zunächst geprüft werden, ob dieser Cookie noch gesetzt ist. Ist er gesetzt wird die Gültigkeit um die konfigurierte Zeitspanne erhöht. Ist der Cookie nicht mehr gesetzt wird der User automatisch abgemeldet.

Der Cookie sollte zusätzlich auch client seitig per JavaScript in einem regelmäßigen Intervall aktualisiert werden, sodass ein aktives Browserfenster (Maus innerhalb des Fensters) den Logout verzögert.

### Mögliche Gefahren

Der Logout wird erzwungen - mit allen Konsequenzen. 

Das schließt zum Beislpiel auch den Fall ein, dass ein Nutzer begonnen hat ein Formular auszufüllen. Verlässt er dann für einen längeren Zeitraum mit der Maus das Fenster und sendet das Formular später ab, werden die Formulardaten verworfen und der Nutzer abgemeldet.

Der Logout kann grundsätzlich nur aus dem Frontend getriggert werden. Backenduser werden in dem Zuge dann auch abgemeldet. Dies kann zu unerwünschten Nebenwirkungen für die Pflege der Inhalte führen, wenn beispielsweise Änderungen noch nicht gespeichert wurden.