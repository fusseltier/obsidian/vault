## Variante 1
### A
Quelle Excel Syneco (ohne Macros, sonst nicht bearbeitet)
#### Aufwand
**ca. 4 - 5 Tage**

### B
Quelle CSV aus Excel Syneco, reduziert auf die notwendigen Daten nach Vorgabe ZWEI14
#### Aufwand
**ca. 3 - 4 Tage**

## Variante 2
Quelle CSV nach Vorgabe ZWEI14
### Aufwand: 
**ca. 2.0 - 2.5 Tage**

## Variante 3
Pflege direkt im TYPO3 Backend
### Aufwand: 
**ca. 1.5 - 2.0 Tage**