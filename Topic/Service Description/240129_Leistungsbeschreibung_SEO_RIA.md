## URL Segmente einsparen

Produkte auf Ebene 1 ziehen, voraussichtlich in Ordner
+ 301 Weiterleitungen (über Shortcut oder über .htaccess)

**Aufwand**: Da hier noch RealURL zum Einsatz kommt, muss ich das ausprobieren. Im prnzip sollte der Aufwand nicht groß sein.

## FAQ

Erstellen eines neuen Templates für die FAQ Unterseiten. Dieses bindet FAQPage Snippet im head Tag ein.

Erstellen eines Akkordion inkl. Rich Snippets für das FAQ. Dieses bindet Question und Answer Snippest im Body (im Akkordion) ein.

**Aufwand 0.5d - 0.75d**

Beispiel:

```html
<!DOCTYPE html>
	<html>
	<head>
	    <!-- Your HTML head content here -->
	
	    <script type="application/ld+json">
	    {
	      "@context": "https://schema.org",
	      "@type": "FAQPage",
	      "mainEntity": []
	    }
	    </script>
	</head>
	<body>
	
	<div class="faq-question">
	    <h2>What is JSON-LD?</h2>
	    <div class="faq-answer">
	        <p>JSON-LD (JavaScript Object Notation for Linked Data) is a lightweight data interchange format used to link data on the web. It is a way to structure data using JSON and provide a common format for sharing data between different systems.</p>
	    </div>
	    <script type="application/ld+json">
	    {
	      "@type": "Question",
	      "name": "What is JSON-LD?",
	      "acceptedAnswer": {
	        "@type": "Answer",
	        "text": "JSON-LD (JavaScript Object Notation for Linked Data) is a lightweight data interchange format used to link data on the web. It is a way to structure data using JSON and provide a common format for sharing data between different systems."
	      }
	    }
	    </script>
	</div>
	
	<!-- Repeat the above structure for other question-answer pairs -->
	
	</body>
	</html>
```

	