#service-description #svs

**Variante 2**

>Wir berechnen die Monatspreise und stellen diese per Excel-Datei zur Verfügung. Somit steht am 1. eines Monats der Preis für den vorangegangenen Monat fest und wird dann monatsweise hinzugefügt und ergänzt.\*

\* Siehe Mail vom 05. Dez. 2022 
  **Betreff:** AW: Visualisierung Website neues Stromprodukt

Unsere Leistungsbeschreibung bezieht sich auf **Variante 2**. 

## Grundsätzliches Vorgehen:

Wir implementieren eine visuelle Darstellung der Preisentwicklung über standardmäßig insgesamt 12 Monate. 

Diese umfasst beim Öffnen der Seite jeweils die 12 aktuellsten Monate, für die über **CSV-Dateien** Daten zum Strompreis an der Börse, sowie den Endkundenpreis der SVS vorliegen. 

Die Visualisierung erfolgt über ein Balkendiagramm. Für jeden Monat werden zwei Balken dargestellt (Strompreis an der Börse (ct/kWh), Endkundenpreis der SVS (ct/kWh)). Die Skala wird in 10 Cent Schritten aufgebaut von 0 - "x" (vgl. Beispiel EGT). Die Skala (bzw. "x" richtet sich nach dem höchsten Preis innerhalb der dargestellten Monate.

Zusätlich wird der Durchschnittspreis über die dargestellten Monate mit einer waagerechten Linie ausgegeben.

Die Darstellung erfolgt voraussichtlich über ein neues TYPO3 Content-Element.

### Zeiteräume

Der Nutzer kann Über ein Drop-Down Menü verschiedene Zeiträume auswählen, für die die Preisentwicklung visuell dargestellt werdern soll:

- 12 Monate
- 18 Monate
- 24 Monate
- etc.

**Die möglichen Zeiträume müssen vorab abgestimmt werden. Diese sind außerdem anhängig von den vorliegenden Daten.**

### Die CSV Dateien werden wie folgt aufgebaut:

Es gibt eine Datei für jedes Jahr - benannt "YYYY.csv" (also bspw. 2022.csv, 2023.csv).  

Jede Datei enthält 12 Spalten und drei Zeilen

- Zeile 1: Monate (1 - 12)
- Zeile 2: Strompreis an der Börse (ct/kWh)
	- Der Preis wir als float Value angegeben (also z.B: 23.323)
- Zeile 3: Strompreis der SVS (ct/kWh)
	- Der Preis wir als float Value angegeben (also z.B: 23.323)

Die einzelnen Spalten werden jeweils mit einem Komma separiert.

Liegen (noch) keine Daten für einen Monat vor, so bleiben die Zeilen 2 und 3 leer.

Die Dateien werden in einem von ZWEI14 bereitgestellten Ordner hochgeladen und ausgelesen.

## Platzierung

Die Visualisierung wird zunächst auf einer dafür neu angelegten Contentseite eingebunden.

*In einem zweiten Schritt kann diese auch auf der jeweiligen Detailseite des neuen Stromtarifs ausgegeben werden. Dies setzt allerdings ein noch zu definierendes Attribut im CSIT Tarif voraus, über das die Anzeige gesteuert werden kann.* **Dieser zweite Schritt ist im geschätzten Aufwand nicht berücksichtigt.**

## Begründung

Die Umsetzung über monatlich aktualisierte Preise ist technisch einfacher. Eine Berechnung wird lediglich für den Durchschnittspreis benötigt. Zudem stimmt der Preis im Konfigurator (monatlich manuell aktualisiert) mit der visuellen Darstellung überein.

## Fragen

Für welchen Zeitraum können die Daten geliefert werden? Auch bspw. für die letzten 24 Monate (auch wenn es den Tarif da noch nicht gab?? 

