#service-description #svs
#service-description #svs

## Fragen vorab:
  
- Wird der neue Tarif auch über CSIT im Konfigurator verfügbar gemacht?
	- Wenn ja, gibt es dort noch irgendwelche Anforderungen an die Darstellung des Preises?
	- Wenn ja, soll die Visualisierung auch auf der Detailseite des Tarifs erscheinen?
- Wo soll die Visualisierung dargestellt werden?
	- Startseite? Evtl. unterhalb vom aktuellen Konfigurator?
	- Bekommt der Tarif inkl. Viualisierung eine eigene Unterseite im Bereich Strom?
- Wird der Tarif für Privat- und Geschäftskunden angeboten?

## Grundsätzliches Vorgehen:

Wir implementieren eine visuelle Darstellung der Preisentwicklung über insgesamt 12 Monate. Diese umfasst jeweils die 12 aktuellsten Monate, für die über CSV Dateien Daten zum Strompreis an der Börse, sowie den Endkundenpreis der SVS vorliegen. Die Visualisierung erfolgt über ein Balkendiagramm. Für jeden Monat werden zwei Balken dargestellt (Strompreis an der Börse (ct/kWh), Endkundenpreis der SVS (ct/kWh)). Die Skala wird in 10 Cent Schritten aufgebaut von 0 - "x" (vgl. Beispiel EGT). Die Skala(bzw. "x" richtet sich nach dem höchsten Preis innerhalb der dargestellten Monate.

### Die CSV Dateien werden wie folgt aufgebaut:

Es gibt eine Datei für jedes Jahr - benannt "YYYY.csv" (also bspw. 2022.csv, 2023.csv).  

Jede Datei enthält 12 Spalten und drei Zeilen

- Zeile 1: Monate (1 - 12)
- Zeile 2: Strompreis an der Börse (ct/kWh)
	- Der Preis wir als float Value angegeben (also z.B: 23.323)
- Zeile 3: Strompreis der SVS (ct/kWh)
	- Der Preis wir als float Value angegeben (also z.B: 23.323)

Die einzelnen Spalten werden jeweils mit einem Komma separiert.

Liegen (noch) keine Daten für einen Monat vor, so bleiben die Zeilen 2 und 3 leer.

Die Dateien werden in einem von ZWEI14 bereitgestellten Ordner hochgeladen und ausgelesen.