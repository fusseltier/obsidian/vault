#with #block
## Block

```php
{% with {
            'myVar1': myValue1,
            'myVar2': myValue2
        }
%}
        {{ block('toolbar', myTemplate) }}
{% endwith %}
```