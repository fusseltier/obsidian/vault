#traefik #certificate #yaml #crt #key
```yaml
# traefik.yml
tls:
  certificates:
    - certFile: /certs/wildcard.selfsignedcert.localhost.crt
      keyFile: /certs/wildcard.selfsignedcert.localhost.key
```

```yaml
# docker-compose.yaml
#######################################
# PHP application Docker container
#######################################
version: "3"
services:
  app:
    build:
      context: .
      dockerfile: ./docker/images/php/Dockerfile
    image: "${ENV_NAMESPACE}/${ENV_PROJECT_NAME}:${ENV_VERSION}"
    volumes:
      - ./mount/app/:/app/
    links:
      - mysql
    ports:
      - "80${ENV_PORT_SUFFIX}:80"
      - "44${ENV_PORT_SUFFIX}:443"
    expose:
      - "9000"
    # cap and privileged needed for slowlog
    cap_add:
      - SYS_PTRACE
    privileged: true
    labels:
    - "traefik.enable=false"
    networks:
      - default
#    restart: always
    env_file: 
      - ./.env

  #######################################
  # httpd
  #######################################
  apache2:
    image: ${ENV_APACHE_IMAGE}
    volumes:
      - ./docker/images/apache2/httpd_dev.conf:/usr/local/apache2/conf/httpd.conf
      - ./docker/images/apache2/httpd-vhosts.conf:/usr/local/apache2/conf/extra/httpd-vhosts.conf
      - ./mount/app/:/app/
      #- ./mount/.composer/:/root/.composer/
      #- ./mount/.ssh/:/root/.ssh/
    # ports:
    #   - ${PROD_APACHE_PORT}:80
    links:
      - app
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.apache2.rule=Host(`${ENV_SERVICE_APACHE_HOST}`)"
      - "traefik.http.routers.apache2.entrypoints=websecure"
      - "traefik.http.routers.apache2.tls=true"
##      - "traefik.http.routers.apache2.tls.certresolver=myresolver"
##      - "traefik.http.routers.apache2.middlewares=authapache2"
##      - "traefik.http.middlewares.authapache2.basicauth.users=${ENV_SERVICE_APACHE2_USER}:${ENV_SERVICE_APACHE2_PASSWORD}" # user/password
    networks:
      - default
#    restart: always

  #######################################
  # MySQL server
  #######################################
  mysql:
    image: ${ENV_MYSQL_IMAGE}
    working_dir: /application
    command: [mysqld, --character-set-server=utf8mb4, --collation-server=utf8mb4_unicode_ci, --innodb-file-format=Barracuda, --innodb-large-prefix=1, --innodb-file-per-table=1]
    volumes:
    - ./mysql_seed/:/docker-entrypoint-initdb.d
    - devdatabase:/var/lib/mysql
    environment:
      - MYSQL_USER=${ENV_MYSQL_USER}
      - MYSQL_DATABASE=${ENV_MYSQL_DATABASE}
      - MYSQL_PASSWORD=${ENV_MYSQL_PASSWORD}
      - MYSQL_ROOT_PASSWORD=${ENV_MYSQL_ROOT_PASSOWRD}
    labels:
      - "traefik.enable=false"
    networks:
      - default
#    restart: always

  #######################################
  # adminer
  #######################################
  adminer:
    image: adminer
    # ports:
    #   - 70${PROD_PORT_SUFFIX}:8080
    labels:
      - "traefik.enable=true"
      - "traefik.http.routers.adminer.rule=Host(`${ENV_SERVICE_ADMINER_HOST}`)"
#      - "traefik.http.routers.adminer.tls.certresolver=myresolver"
      - "traefik.http.routers.adminer.tls=true"
      - "traefik.http.routers.adminer.entrypoints=websecure"
##      - "traefik.http.routers.adminer.middlewares=authadminer"
##      - "traefik.http.middlewares.authadminer.basicauth.users=${ENV_SERVICE_ADMINER_USER}:${ENV_SERVICE_ADMINER_PASSWORD}" # user/password
#    restart: always

  reverse-proxy:
    image: docker.io/traefik:v2.5
    container_name: docker-traefik
    ports:
       - "80:80"
       - "443:443"
    command:
      - "--log.level=DEBUG"
      - "--log.filePath=/var/log/traefik/debug.log"
      - "--api=true"
      - "--providers.docker=true"
      - "--providers.docker.exposedbydefault=false"
      - "--providers.file.directory=/etc/traefik/dynamic_conf"
      - "--providers.file.watch=true"
      - "--entrypoints.web.address=:80"
      - "--entrypoints.websecure.address=:443"
#      - "--certificatesresolvers.myresolver.acme.httpchallenge=true"
#      - "--certificatesresolvers.myresolver.acme.httpchallenge.entrypoint=web"
#      #- "--certificatesresolvers.myresolver.acme.caserver=https://acme-staging-v02.api.letsencrypt.org/directory"
#      - "--certificatesresolvers.myresolver.acme.email=${ENV_SERVICE_TRAEFIK_ACME_EMAIL}"
#      - "--certificatesresolvers.myresolver.acme.storage=/acme.json"
    volumes:
#      - ./acme.json:/acme.json
      - ./logs:/var/log/traefik
      - /var/run/docker.sock:/var/run/docker.sock:ro
      - ./certs:/certs/:ro
      - ./traefik.yml:/etc/traefik/dynamic_conf/conf.yml:ro
    labels:
      - "traefik.enable=true"
      # Dashboard
      - "traefik.http.routers.traefik.rule=Host(`${ENV_SERVICE_TRAEFIK_HOST}`)"
      - "traefik.http.routers.traefik.service=api@internal"
#      - "traefik.http.routers.traefik.tls.certresolver=myresolver"
      - "traefik.http.routers.traefik.tls=true"
      - "traefik.http.routers.traefik.entrypoints=websecure"
##      - "traefik.http.routers.traefik.middlewares=authtraefik"
##      - "traefik.http.middlewares.authtraefik.basicauth.users=${ENV_SERVICE_TRAEFIK_USER}:${ENV_SERVICE_TRAEFIK_PASSWORD}" # user/password

      # global redirect to https
      - "traefik.http.routers.http-catchall.rule=hostregexp(`{host:.+}`)"
      - "traefik.http.routers.http-catchall.entrypoints=web"
      - "traefik.http.routers.http-catchall.middlewares=redirect-to-https"

      # middleware redirect
      - "traefik.http.middlewares.redirect-to-https.redirectscheme.scheme=https"
    networks:
      - default
#    restart: always

  #######################################
  # phpmyadmin
  #######################################
  # phpmyadmin:
  #   image: phpmyadmin
  #   ports:
  #     - 60${PROD_PORT_SUFFIX}:80
  #   environment:
  #     - PMA_ARBITRARY=1
  #   labels:
  #   - "traefik.enable=false"

  #######################################
  # node and gulp
  #######################################
#  phpgulp:
#    build:
#      context: .
#      dockerfile: ./docker/images/gulp/Dockerfile
#    volumes:
#      - ./mount/app/:/app/
#    ports:
#      - 90${PROD_APP_PORT_SUFFIX}:80

volumes:
  devdatabase:

# docker network create zwei14gmbhxxxdev --subnet 172.36.0.1/24
networks:
  default:
    name: ${ENV_NAMESPACE}${ENV_PROJECT_NAME}${ENV_VERSION}
```