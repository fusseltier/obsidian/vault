https://pimcore.com/docs/platform/Pimcore/Best_Practice/Modifying_Permissions_based_on_Object_Data

https://pimcore.com/docs/platform/Pimcore/Extending_Pimcore/Add_Your_Own_Permissions

https://pimcore.com/docs/platform/Pimcore/Extending_Pimcore/Bundle_Developers_Guide/Event_Listener_UI

```JavaScript
pimcore.registerNS("pimcore.plugin.Z14PmcrCustomBundle");

  

pimcore.plugin.Z14PmcrCustomBundle = Class.create({

  

initialize: function () {

document.addEventListener(pimcore.events.pimcoreReady, this.pimcoreReady.bind(this));

  

document.addEventListener(pimcore.events.postOpenObject, (e) => {

if (e.detail.object.data.general.className === 'CsiProduct') {

  

if(pimcore.currentuser.permissions.indexOf("my_permission") >= 0) {

//...

}

  

e.detail.object.toolbar.add({

id: 'deepl_translate_from_de',

text: t('LLL.translateFromDe'),

iconCls: 'pimcore_icon_language_de',

scale: 'small',

handler: function (obj) {

//do some stuff here, e.g. open a new window with an PDF download

console.log(obj);

console.log(obj.edit);

console.log(pimcore.currentuser.permissions);

}.bind(this, e.detail.object)

});

e.detail.object.toolbar.add({

id: 'deepl_translate_from_en',

text: t('LLL.translateFromEn'),

iconCls: 'pimcore_icon_language_en',

scale: 'small',

handler: function (obj) {

//do some stuff here, e.g. open a new window with an PDF download

console.log(obj);

var pimcorePanelTabs = document.getElementById("pimcore_panel_tabs");

// Create a new <div> element

var newDiv = document.createElement("div");

newDiv.setAttribute("style", "position:fixed; top:0; left:0; width:100%; height:100%; background-color:rgba(0,0,0,0.75); z-index:99999;");

  

// Set some content for the new div

newDiv.textContent = "Prepended div at: " + new Date().toLocaleTimeString();

  

// Prepend the new div to the contentDiv

pimcorePanelTabs.insertBefore(newDiv, pimcorePanelTabs.firstChild);

}.bind(this, e.detail.object)

});

pimcore.layout.refresh();

}

});

  

document.addEventListener(pimcore.globalLanguage.changed, (e) => {

if (e.detail.object.data.general.className === 'CsiProduct') {

console.log(e);

}

});

  

},

  

pimcoreReady: function (e) {

// alert("Z14PmcrCustomBundle ready!");

}

});

  

var Z14PmcrCustomBundlePlugin = new pimcore.plugin.Z14PmcrCustomBundle();
```