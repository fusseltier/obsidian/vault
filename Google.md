##  Tagmanager

```javascript
<script type="text/plain" data-consent="statistical" data-consent-id="googletagmanagerWithConsent">
(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-XXXXXXX');
</script>
```

```javascript
<script>
	window.addEventListener("load", function () {
		if (document.cookie.split(';').some(function(item) {
			if(item.indexOf('cookiemanager_selection=') >= 0){
				if(item.indexOf(',statistical') >= 0){
					(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0], j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src= 'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f); })(window,document,'script','dataLayer','GTM-TPTG945');
				}
			}
		})) {}
	});
</script>
```

## Analytics

````javascript
window.addEventListener("load", function () {

        if (document.cookie.split(';').some(function(item) {
            if(item.indexOf('cookiemanager_selection=') >= 0){
                if(item.indexOf(',statistical') >= 0){

                    var gtagScript = document.createElement("script");
                    gtagScript.type = "text/javascript";
                    gtagScript.setAttribute("async", "true");
                    gtagScript.setAttribute("src", "https://www.googletagmanager.com/gtag/js?id=G-N2R434SW3V");
                    document.documentElement.firstChild.appendChild(gtagScript);

                    window.dataLayer = window.dataLayer || [];
                    function gtag(){dataLayer.push(arguments);}
                    gtag('js', new Date());

                    gtag('config', 'G-N2R434SW3V', { 'anonymize_ip': true });
                }
            }
        })) {}
    });
```