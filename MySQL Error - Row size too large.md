---
tags:
  - mysql
  - sql
  - error
---
## Error 

> Database update failed
> Error: Row size too large. The maximum row size for the used table type, not counting BLOBs, is 8126. This includes storage overhead, check the manual. You have to change some columns to TEXT or BLOBs

## Solution

```
ALTER TABLE tablename ROW_FORMAT=DYNAMIC;
```
