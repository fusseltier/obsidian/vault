- Tickets ohne Beschreibung (No Go)
- Abläufe: 
	- Projekte ziehen sich ewig, man geht tausendmal dran
	- Besprechungen, Konzeption ohne Digital
		- Man wird im gesamten Projektverlauf quasi nie abgeholt
	- Briefings unter aller Kanone
		- Figma mit tausen Entwürfen, keine klare Struktur
		- Nicht klar ob und was freigegeben wurde