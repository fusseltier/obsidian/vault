---
tags:
---
Use `pidInList.field = pid` in `TYPO3\CMS\Frontend\DataProcessing\DatabaseQueryProcessor` query to get IRRE elements from the page where the parent element lives.

Otherwise `pid` in query is the `uid` of the current page. So e.g. you can not use an accordion from another page in a lib.

## Example

	tt_content.ce_accordion.dataProcessing.20.pidInList.field = pid