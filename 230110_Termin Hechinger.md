- [x]  Announcements Appointments Neueste Elemente im Seitenbaum oben, nach Titel
- [ ] Evtl Personalnummer, Firma
	- [x] Import beim Login
	- [x] Evtl. im Form als Vorauswahl/nicht editierbar wie Firstname, Lastname, etc.
- [ ] Formular Export der Daten als CSV? zusätzlich


- [x] Produktfilter Standort prüfen
	- [x] Standorte nur Schwenningen und Dauchingen

- Im User werden beim Login nun u.a. auch die Felder gespeichert:
	- Office location
	- Extension attribute 1 - 15
- Korrektur Standort im Produktfilter
- Sortierung der Announcements und Appointments absteigend nach Titel
- Im Textfeld k"onnen nun auch alle Extension Attribute Felder (also auch die Personalnummer) des aktiven Nutzers vorausgew"ahlt werden.
