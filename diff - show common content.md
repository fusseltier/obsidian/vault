```bash
diff -r --exclude=.git --side-by-side a b | grep -n -v "[|<>]"
```

where `a` and `b` are folders.

#bash #diff #grep 