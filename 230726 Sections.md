## Der Section Ansatz
### Was ist das?

Eine Section besteht aus zuvor festgelegten Feldern. Diese werden auf Grundlage gestalteten Sections passgenau für den Kunden definiert. 

In der Regel sind das beispielsweise:

- Layout der Section (Layout)
- Headline 
- Art der Headline (H1 - H6)
- Subline
- Hintergrundfarbe (vordefiniert)
- Text (WYSIWYG)
- Link/CTA (File, Page, Externe URL)
- Single Media (jpg, png, webp, svg, mp4)
- Media (jpg, png, webp, svg, mp4)
- Files (pdf, docx, ...)
 
Eine Section definiert und garantiert eine optimale Darstellung der gepflegten Inhalte über alle Geräte hinweg. Bspw. werden passgenaue Abstände, Schriftarten, Schriftgrößen, Positionierung von Bild und Text definiert.

Im Idealfall können alle Sections mit diesem einen Set an Feldern bzw. einem Teil der Felder gepflegt werden. Nicht benötigte Felder können im Backend je Section Layout ein oder ausgeblendet werden.

Alle Seiten werden aus einem Füllhorn an vordefinierten Sections erstellt. Die Möglichkeiten unterschiedliche, dennoch konsistente Seiten zu generieren sind schier unerschöpflich.

## Vorteile in der Content Pflege

- Hohe Flexibilität im Aufbau jeder Seite. Durch die Auswahl aus einer Fülle von Sections sind im Seitenaufbau nahezu keine Grenzen gesetzt.
- Einfache Pflege. Volle Konzentration auf den Inhalt.
- Einfacher Wechsel zwischen verschiedenen Sections. Der bereits gepflegte Content bleibt beim Wechsel bestehen. So kann nachträglich die beste Darstellung für den Inhalt gewählt werden. Beispielsweise, wenn sich die Menge des Fließtextes stark verändert.

## Strukturelle Vorteile

- Weitgehende Trennung von Inhalt und Darstellung.
- Einheitlicher Look über die gesamte Webseite.
- Neue Sections können bei Bedarf jederzeit erstellt werden. Dafür ist in der Regel lediglich ein weiteres Section Template notwendig.
- Die Webseite kann organisch wachsen und weiterentwickelt werden. So kann ein Refresh der Optik in weiten Teilen unabhängig vom Inhalt durchgefḧrt werden. 

## Seiten Typen

Über vordefinierte Page Typen bseispielsweise für Produktseiten können die Standard Seitentypen von TYPO3 um benötigte Felder flexibel erweitert werden. So können Inhalte wie Teaser Texte, Bilder, etc. beispielsweise für verschiedene Menütypen definiert und gepflegt werden.