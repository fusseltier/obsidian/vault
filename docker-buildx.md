
```
sudo pacman -S docker-buildx
```

```
docker buildx create --name multi-arch \\
  --platform "linux/arm64/v8,linux/amd64,linux/amd64/v2,linux/amd64/v3,linux/386" \\
  --driver "docker-container"
```

```
buildx ls
```

```
docker buildx use multi-arch
```

```
export DOCKER_TMPDIR=$(pwd) && docker buildx build --platform linux/amd64,linux/arm64/v8 -t foo/bar:baz --force-rm .
```