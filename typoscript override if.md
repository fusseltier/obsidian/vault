#typo3 #typoscript #override #if #condition 

```
tt_content.stdWrap.dataWrap.override = |
tt_content.stdWrap.dataWrap.override.if {
    value = t3template_box
    equals.field = CType
}
```

Nesting of Typoscript "if"-conditions is possible by using the stdWrap-properties of the conjunctions. In your case the docs says, conditions are connected with an AND conjunction. So you just need to use the stdWrap-properties of another condition (lets take isTrue) and make it an TEXT object by using the cObject property of stdWrap. Then you can place another "equals" condition inside the cObject which is returning "1" to isTrue if the condition is met.

**Try it like this, not tested**

```
20 = TEXT
20 {
    value = <div class="div1">|</div>
    value {
        override = <div class="div2">|</div>
        override.if {
            # Condition 1
            value = 10
            equals.field = colPos
            # Another "equals" condition nested in isTrue using cObject
            isTrue.cObject = TEXT
            isTrue.cObject {
                value = 1
                if.value = textmedia
                equals.field = CType
            }
        }
    }
}
```

Creating an OR-condition in Typocript is also possible. You can convert the isTrue property to an COA holding multiple TEXT objects which are returning something like 1 if their conditions are met.

**Example for creating an OR:**

```
20 = TEXT
20 {
    value = <div class="div1">|</div>
    value {
        override = <div class="div2">|</div>
        override {
            if.isTrue.cObject = COA
            if.isTrue.cObject {
                10 = TEXT
                10 {
                    value = 1
                    if.value = 10
                    if.equals.field = colPos
                }
                20 = TEXT
                20 {
                    value = 1
                    value = textmedia
                    equals.field = CType
                }
            }
        }
    }
}
```

Some helpful docs about this:

[https://docs.typo3.org/typo3cms/TyposcriptReference/Functions/If.html](https://docs.typo3.org/typo3cms/TyposcriptReference/Functions/If.html)

[https://docs.typo3.org/typo3cms/TyposcriptReference/Functions/Stdwrap.html](https://docs.typo3.org/typo3cms/TyposcriptReference/Functions/Stdwrap.html)
