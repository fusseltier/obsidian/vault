#data #import #hub
## Data Hub
https://pimcore.com/docs/platform/Datahub/

### Features in a Nutshell[​](https://pimcore.com/docs/platform/Datahub/#features-in-a-nutshell "Direct link to Features in a Nutshell")
- Easy-to-configure interface layer for data delivery and consumption
- Tool of choice to connect Pimcore to any other systems and applications besides internal PHP API - whether they are backend applications like ERP systems or frontend applications like your storefront
- Multiple endpoints definition for different use cases and target/source systems
- Central and easy-to-use GUI to transform and prepare data for defined endpoints
- To-be-exposed data restriction to endpoints by defining workspaces and schemas.

### Data Importer
https://pimcore.com/docs/platform/Data_Importer/

#### Features in a Nutshell[​](https://pimcore.com/docs/platform/Data_Importer/#features-in-a-nutshell "Direct link to Features in a Nutshell")
- Multiple imports configuration directly in Datahub.
- Data import from various data sources.
- Supported File Formats: `csv`, `xlsx`, `json`, `xml`.
- Strategies configuration for:
    - loading existing elements for updating data.
    - defining location for newly imported data.
    - publishing data.
    - cleanup of existing data.
- Mappings definition for adjusting data to Pimcore Data Objects with:
    - simple transformations.
    - preview of imported data.
- Imports execution directly in Pimcore Datahub or on a regular base via cron definitions.
- Import status updates and extensive logging information.

## Grundsätzliche Überlegungen
- Technische Basis: Pimcore 11
- Sage als Single Point of Truth
	- Export von Sage nach Pimcore über den Data Importer bzw. das Data Hub, beides Core Extensions von Pimcore
	- Pimcore erweitert die Daten um SEO/Web relevante Felder, die nicht von Sage überschrieben werden
	- Kein Abgleich von Pimcore nach Sage
	- Sage Datenstruktur schon bei der Übernahme aus TYPO3 (Step 1) berücksichtigen
- Filter Attribut Values bisher ausschließlich Strings
	- Machen andere Datentypen Sinn?
		- Integer, Floats, Booleans (Abhängig vom konkreten Anwendungsfall)
- Deeple API für Übersetzungen
	- https://www.deepl.com/docs-api/translate-text/multiple-sentences
	- https://github.com/DeepLcom/deepl-php
	- https://www.deepl.com/pro#developer
		- Es entstehen Kosten
	- Fehlende Sprachen in Deepl:
		- KR
		- ZA

## Rekapitulation Call mit Herr Wehrle
- Alle Daten in Sage
- REST API vorhanden (aber keine Erfahrungen bisher damit bei CSI)
- Varianten können In Sage schrittweise gefiltert werden über Regeln (Code in mir unbekannter Sprache), sehr komplex insgesamt
	- Evtl. Ein Export einer Matrix der Regeln als Excel
- Ziel möglicherweise: Varianten über Filter analog zu Sage auf der Webseite finde, anfragen und Anfrage Parameter als XML für ein Angebot in Sage einspielen
	- Aktuell bedarf eine Anfrage meiste einer Mailschlacht mit viel Rückfragen etc.