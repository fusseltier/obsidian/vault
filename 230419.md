## Abmelden im Intranet soll den Nutzer auch aus dem Microsoft Konto (AAD) abmelden

Technisch dürfte sich das realisieren lassen.

Wir raten jedoch davon ab.

### Was macht ein automatische Abmelden auch aus AAD problematisch?

Angenommen ein Mitarbeiter nutzt Teams und Office 365 und ist bei Microsoft angemeldet. Er wechselt ins Intranet und meldet sich dort nach der Nutzung ab. Anschließend muss der Nutzer sich bspw. in Office 365 erneut anmelden, da die Abmeldung im Intranet auch aus AAD abmeldet, obwohl er sich zuvor dort exlizit nicht abgemeldet hatte.

Grundsätzlich würde ich als Nutzer das Abmelden aus dem Microsoft Konto (AAD) nur beim Logout aus einer Microsoft App (wie teams, Office 365 oder Sharepoint) erwarten, nicht jedoch bei einer "Drittanbieter" App wie dem Intranet. 

Es ergeben sich unter anderem diese Probleme:

- Die Akzeptanz des Intranets könnte sinken. Nutzer melden sich seltener im Intranet an, da sie nach dem Abmelden dort anschließend genötigt werden sich in anderen Apps wie Office 365 erneut (möglicherweise sogar mit 2FA) anzumelden. 
- Der Vorteil, dass ein Nutzer sich nach erfolgreichem Login im intranet auch in anderen Apps ohne Eingabe seines Passworts und ohne 2FA anmelden kann geht verloren, wenn er sich im Intranet abmeldet.
- Der Vorteil eines Single Sign On, dass ein Nutzer zwischen verschiedenen Apps wechseln kann ohne sich mehrfach anzumelden, wird durch den kompletten Logout konterkariert. Möglicherweise melden sich Nutzer aus dem Intranet erst gar nicht mehr ab, sondern schließen nur noch den Browser, um den kompletten Logout bei Microsoft zu umgehen.

Wenn Sie dennoch ein Angebot von uns wollen, kalkulieren wir den Aufwand gerne für Sie.

