# indexed_search

Standard Extension von TYPO3.

Besteht aus zwei Komponenten:
- Indexer
- Suche

Der Indexer kann über eine Crawler Extension regelmäßig aufgerufen werden.

## Features Indexer

[Dokumentation](https://docs.typo3.org/c/typo3/cms-indexed-search/11.5/en-us/Introduction/Index.html#features-of-the-indexer) 

### Priorisierung der inhalte in der dargestellten Reihenfolge:

-  Metadaten
	- \<title\>
	- \<meta-keywords\>
	- \<meta-description\>
- Inhalt
	- \<body\> (Content)

### Index

- Indexierung von Dateien: Text Formate wie html, txt, doc, pdf über externe programme (catdoc / pdftotext)
- Wortzählung und Häufigkeit zur Bewertung der Ergebnisse
- Exakte, partielle oder metaphone Suche
- Freies Suchen nach Sätzen (nicht indexiert)
- NICHT case-sensitive

## Features Suche (the plugin)

[Dokumentation](https://docs.typo3.org/c/typo3/cms-indexed-search/11.5/en-us/Introduction/Index.html#features-of-the-search-frontend-the-plugin)

Die Suchoberfläche bietet mehrere Optionen für die erweiterte Suche. Alle davon können deaktiviert und/oder mit Standardwerten voreingestellt werden:

- Suche nach ganzen Wörtern, Wortteilen, Klängen wie Sätzen
- Logische UND- und ODER-Suche einschließlich syntaktischer Erkennung von UND, ODER und NICHT als logische Schlüsselwörter. Außerdem werden in Anführungszeichen gesetzte Sätze erkannt.
- Die Suche kann in bestimmten Abschnitten der Website durchgeführt werden. (Wird im Template definiert, z.B. können Footer und Menü ausgenommen werden)
- Die Ergebnisse können absteigend oder aufsteigend sortiert und nach Worthäufigkeit, Gewichtung, Position relativ zum Seitenanfang, Änderungsdatum der Seite, Seitentitel usw. sortiert werden.